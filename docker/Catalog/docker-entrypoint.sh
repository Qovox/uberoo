#!/bin/sh

while ! mysqladmin ping -h $DB_HOST --silent; do
    echo "Waiting for database connection..."
    sleep 3
done

configureDB.sh 

asadmin start-domain --verbose