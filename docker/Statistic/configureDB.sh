#!/bin/sh
mkdir tmp
mv $GLASSFISH_HOME/glassfish/domains/domain1/autodeploy/statistic.war ./tmp
cd ./tmp
unzip statistic.war
cd WEB-INF/classes/

echo "jdbc.url=jdbc:mysql://$DB_HOST:3306/"> configDB.properties
echo "jdbc.databaseName=$DB_NAME">> configDB.properties
echo "jdbc.user=$DB_USER">> configDB.properties
echo "jdbc.password=$DB_PASSWORD">> configDB.properties

cd ../..
zip -r statistic.war WEB-INF/classes/configDB.properties

mv statistic.war $GLASSFISH_HOME/glassfish/domains/domain1/autodeploy/
cd ..
rm -r tmp