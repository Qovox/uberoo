package simulations

import java.util.UUID

import scala.language.postfixOps

import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._


class courierSimulation extends Simulation {

  val httpConf =
    http
      .baseURL("http://localhost:8081/courier")
      .acceptHeader("application/json")
      .header("Content-Type", "application/json")
  val headers_10 = Map("Content-Type" -> """application/x-www-form-urlencoded""")
  val stressSample =
    scenario("Courier")
        .repeat(10)
        {
          exec(
            http("Get near orders")
              .get("/nearOrders?address=54%20route%20des%20acacias")
              .check(status.is(200))
          )
          .pause(1 seconds)
          .exec(
            http("Get my orders")
              .get("/2/orderList")
              .check(status.is(200))
          )
          .pause(1 seconds)
          .exec(
            http("Pick order")
              .post("/pickOrderToDeliver")
              .headers(headers_10)
              .formParam("courierId", "4")
              .formParam("orderId", "2")
              .check(status.is(200))
          )
          .pause(1 seconds)
          .exec(
            http("Deliver order")
              .post("/notifyOrderDelivered")
              .headers(headers_10)
              .formParam("courierId", "4")
              .formParam("orderId", "2")
              .check(status.is(200))
          )
        }

  setUp(stressSample.inject(rampUsers(20) over (10 seconds)).protocols(httpConf))
}