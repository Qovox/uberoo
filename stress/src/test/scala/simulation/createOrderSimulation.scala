package simulations

import java.util.UUID

import scala.language.postfixOps

import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._


class createOrderSimulation extends Simulation {

  val httpConf =
    http
      .baseURL("http://localhost:8083/order")
      .acceptHeader("text/xml")
      .header("Content-Type", "text/xml")

  val stressSample =
    scenario("Create order")
        .repeat(10)
        {
          exec(
            http("Create order")
              .post("/OrderService")
              .body(StringBody(createOrder()))
              .check(status.is(200))
          )
        }

  def createOrder(): String = {
    val customerName = randomString(10)
    raw"""<?xml version="1.0" ?>
	<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
		<soapenv:Body>
			<pub:process xmlns:pub="http://informatique.polytech.unice.fr/soa1/order/public">
				<!--Optional:-->
         <event xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="pub:create_order">
         	<order>
         	 <customerName>$customerName</customerName>
         	 <productIds>
         	 	<productId>5</productId>
         	 </productIds>
         	 <adress>5 rue Michel 06560 Valbonne</adress>
         	 <date>2018-10-11 10:47:35</date>
         	 </order>
         </event>
    		</pub:process>
		</soapenv:Body>
	</soapenv:Envelope>
    }"""
  }

  def randomString(length: Int) = {
    val r = new scala.util.Random
    val sb = new StringBuilder
    for (i <- 1 to length) {
      sb.append(r.nextPrintableChar)
    }
    sb.toString
  }

  setUp(stressSample.inject(rampUsers(20) over (10 seconds)).protocols(httpConf))

}