package simulations

import java.util.UUID

import scala.language.postfixOps

import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._


class browseCatalogSimulation extends Simulation {

  val httpConf =
    http
      .baseURL("http://localhost:8080/catalog")
      .acceptHeader("application/xml")
      .header("Content-Type", "application/xml")

  val stressSample =
    scenario("Browse Catalog")
        .repeat(10)
        {
          exec(
            http("Browse catalog")
              .get("/categories")
              .check(status.is(200))
          )
          .pause(1 seconds)
          .exec(
            http("Browse item")
              .get("/categories/Chinese")
              .check(status.is(200))
          )
        }

  setUp(stressSample.inject(rampUsers(20) over (10 seconds)).protocols(httpConf))
}