#!/bin/sh

HOST_CATALOG="http://localhost:8080/catalog"
HOST_COURIER="http://localhost:8081/courier"
HOST_ETA="http://localhost:8082/eta"
HOST_ORDER="http://localhost:8083/order"
HOST_STATS="http://localhost:8084/statistic"
HOST_PAYMENT="http://localhost:8085/payment"
HOST_GEOTRACK="http://localhost:8086/geotracking"



echo "###############################################"
echo "Scenario is starting"
echo "###############################################"

waitService() {
    local response=`curl --write-out %{http_code} --silent --output /dev/null  $1`
    while [ $response -ne "200" ] && [ $response -ne "301" ]; do
        echo "Waiting for service $2..."
        sleep 3
        response=`curl --write-out %{http_code} --silent --output /dev/null  $1`
    done
}

waitService $HOST_CATALOG catalog
waitService $HOST_COURIER courier
waitService $HOST_ETA eta
waitService $HOST_ORDER order
waitService $HOST_STATS statistic
waitService $HOST_PAYMENT payment
waitService $HOST_GEOTRACK geotracking

echo "All service is up\n"

echo "###############################################"
echo "Gail, student"
echo "Erin, software developer"
echo "Jordan, chinese restaurant chef"
echo "Allen, japanese restaurant chef"
echo "Jamie, courier"
echo "Bob, courier"
echo "Terry, chinese restaurant owner"
echo "Steven, japanese restaurant owner"
echo "###############################################"
echo "\n"

echo "###############################################"
echo "Steven emit a promotional code for entry-main course order with 50% discount"
echo "###############################################"
curl -w "\n\n" --header "Content-Type:application/xml" --data @scenario/steven_new_promocode.xml $HOST_CATALOG/promoCodes 

echo "###############################################"
echo "Erin/Gail browse the food catalogue offered by Uberoo"
echo "###############################################"
curl -w "\n\n" -X GET $HOST_CATALOG/categories

echo "###############################################"
echo "Erin decide to go for a sweet sour meal and a starter, Gail decide to go for a chinese meal,"
echo "###############################################"
curl -w "\n\n" -X GET $HOST_CATALOG/categories/Sweet_sour
curl -w "\n\n" -X GET $HOST_CATALOG/categories/Starter

echo "###############################################"
echo "Gail decide to go for a chinese meal,"
echo "###############################################"
curl -w "\n\n" -X GET $HOST_CATALOG/categories/Chinese

echo "###############################################"
echo "Erin browse the Sushi salad and the Sweat and Sour Pork, Gail browse the Ma Po Tofu"
echo "###############################################"
curl -w "\n\n" -X GET $HOST_CATALOG/items/16
curl -w "\n\n" -X GET $HOST_CATALOG/items/17

echo "###############################################"
echo "Gail browse the Ma Po Tofu"
echo "###############################################"
curl -w "\n\n" -X GET $HOST_CATALOG/items/2

echo "###############################################"
echo "The system estimates the ETA for the Sweat and Sour Pork"
echo "###############################################"
curl -w "\n\n" --header "Content-Type:text/xml" --data @scenario/estimate_pork.xml $HOST_ETA/EtaRPCService

echo "###############################################"
echo "The system estimates the ETA for the Sushi salad"
echo "###############################################"
curl -w "\n\n" --header "Content-Type:text/xml" --data @scenario/estimate_salad.xml $HOST_ETA/EtaRPCService

echo "###############################################"
echo "The system estimates the ETA for the Ma Po Tofu"
echo "###############################################"
curl -w "\n\n" --header "Content-Type:text/xml" --data @scenario/estimate_tofu.xml $HOST_ETA/EtaRPCService

echo "###############################################"
echo "Gail decides to confirm the order"
echo "###############################################"
curl -w "\n\n" --header "Content-Type:text/xml" --data @scenario/gail_create_order.xml $HOST_ORDER/OrderService

echo "###############################################"
echo "Erin decides to confirm the order"
echo "###############################################"
curl -w "\n\n" --header "Content-Type:text/xml" --data @scenario/erin_create_order.xml $HOST_ORDER/OrderService

echo "###############################################"
echo "Erin pay directly by credit card on the platform"
echo "###############################################"
sleep 5
curl -w "\n\n" --header "Content-Type:text/xml" --data @scenario/erin_pay_online.xml $HOST_PAYMENT/PaymentService

echo "###############################################"
echo "Gail pay by cash"
echo "###############################################"
curl -w "\n\n" --header "Content-Type:text/xml" --data @scenario/gail_pay_cash.xml $HOST_PAYMENT/PaymentService

echo "###############################################"
echo "Jordan consults the list of meals to prepare"
echo "###############################################"
curl -w "\n\n" --header "Content-Type:text/xml" --data @scenario/jordan_read_restaurant_order.xml $HOST_ORDER/OrderService

echo "###############################################"
echo "Allen consults the list of meals to prepare"
echo "###############################################"
curl -w "\n\n" --header "Content-Type:text/xml" --data @scenario/allen_read_restaurant_order.xml $HOST_ORDER/OrderService

echo "###############################################"
echo "Jordan starts the cooking process"
echo "###############################################"
curl -w "\n\n" --header "Content-Type:text/xml" --data @scenario/jordan_cooking.xml $HOST_ORDER/OrderService

echo "###############################################"
echo "Allen starts the cooking process"
echo "###############################################"
curl -w "\n\n" --header "Content-Type:text/xml" --data @scenario/allen_cooking.xml $HOST_ORDER/OrderService

echo "###############################################"
echo "Jordan has just finished the cooking process"
echo "###############################################"
curl -w "\n\n" --header "Content-Type:text/xml" --data @scenario/jordan_ready_to_deliver.xml $HOST_ORDER/OrderService

echo "###############################################"
echo "Allen has just finished the cooking process"
echo "###############################################"
curl -w "\n\n" --header "Content-Type:text/xml" --data @scenario/allen_ready_to_deliver.xml $HOST_ORDER/OrderService

echo "###############################################"
echo "Jamie search the orders that are ready to be delivered around him"
echo "###############################################"
curl -w "\n\n" -X GET $HOST_COURIER/nearOrders?address=5%20rue%20Michel%2006560%20Valbonne

echo "###############################################"
echo "Jamie pick Erin's order"
echo "###############################################"
curl  -w "\n\n" --header "Content-Type: application/x-www-form-urlencoded" -d "courierId=4&orderId=5"  -X POST $HOST_COURIER/pickOrderToDeliver

echo "###############################################"
echo "Jamie inform that he can't terminate the course for some reason"
echo "###############################################"
curl  -w "\n\n" --header "Content-Type: application/x-www-form-urlencoded" -d "courierId=4&orderId=5"  -X POST $HOST_COURIER/notifyProblem

echo "###############################################"
echo "Bob pick Erin's order"
echo "###############################################"
curl  -w "\n\n" --header "Content-Type: application/x-www-form-urlencoded" -d "courierId=3&orderId=5"  -X POST $HOST_COURIER/pickOrderToDeliver

echo "###############################################"
echo "Jamie pick Gail's order"
echo "###############################################"
curl  -w "\n\n" --header "Content-Type: application/x-www-form-urlencoded" -d "courierId=4&orderId=4"  -X POST $HOST_COURIER/pickOrderToDeliver

echo "###############################################"
echo "Gail track the geolocation of the coursier"
echo "###############################################"
curl -w "\n\n" -X GET $HOST_GEOTRACK/trackOrder?orderId=4

echo "###############################################"
echo "Erin track the geolocation of the coursier"
echo "###############################################"
curl -w "\n\n" -X GET $HOST_GEOTRACK/trackOrder?orderId=5

echo "###############################################"
echo "Jamie delivered the orders"
echo "###############################################"
curl  -w "\n\n" --header "Content-Type: application/x-www-form-urlencoded" -d "courierId=4&orderId=4"  -X POST $HOST_COURIER/notifyOrderDelivered

echo "###############################################"
echo "Bob delivered the orders"
echo "###############################################"
curl  -w "\n\n" --header "Content-Type: application/x-www-form-urlencoded" -d "courierId=3&orderId=5"  -X POST $HOST_COURIER/notifyOrderDelivered

echo "###############################################"
echo "Erin review the meals"
echo "###############################################"
curl -w "\n\n" --header "Content-Type:text/xml" --data @scenario/erin_review.xml $HOST_ORDER/OrderService

echo "###############################################"
echo "Gail review the meals"
echo "###############################################"
curl -w "\n\n" --header "Content-Type:text/xml" --data @scenario/gail_review.xml $HOST_ORDER/OrderService

echo "###############################################"
echo "Jordan check the review"
echo "###############################################"
curl -w "\n\n" --header "Content-Type:text/xml" --data @scenario/jordan_read_review.xml $HOST_ORDER/OrderService

echo "###############################################"
echo "Allen check the review"
echo "###############################################"
curl -w "\n\n" --header "Content-Type:text/xml" --data @scenario/allen_read_review.xml $HOST_ORDER/OrderService

echo "###############################################"
echo "Terry check statistics"
echo "###############################################"
echo "Average Delivery Time"
curl -w "\n\n" -X GET $HOST_STATS/avgDeliveryTime/1
echo "Statistic about couriers"
curl -w "\n\n" -X GET $HOST_STATS/deliveryPerCoursier/1

echo "###############################################"
echo "Steven check statistics"
echo "###############################################"
echo "Average Delivery Time"
curl -w "\n\n" -X GET $HOST_STATS/avgDeliveryTime/9
echo "Statistic about couriers"
curl -w "\n\n" -X GET $HOST_STATS/deliveryPerCoursier/9