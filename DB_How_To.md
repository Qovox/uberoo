# Setup Database

First download MySQL at https://dev.mysql.com/downloads/installer/
(Maybe you'll have to install Python)

The basis tutorial is here: https://www.codejava.net/frameworks/hibernate/java-hibernate-jpa-annotations-tutorial-for-beginners

Fist command:
```
create database usersdb;
```

Second command:

```sql
CREATE TABLE `users` (
	`user_id` int(11) NOT NULL AUTO_INCREMENT,
	`fullname` varchar(45) NOT NULL,
	`email` varchar(45) NOT NULL, 
	`password` varchar(45) NOT NULL, 
	PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
```

```sql
CREATE TABLE `items` (
	`id` int(10) NOT NULL AUTO_INCREMENT,
	`name` varchar(45) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
```

Next step is annotating the pojo in the code.

Finally we create the persistence.xml file as following:
```xml
<properties>
	<property name="javax.persistence.jdbc.url" value="jdbc:mysql://localhost:3306/catalogdb"/>
	<!-- specifies the username of the account having privilege to access to the database. -->
	<property name="javax.persistence.jdbc.user" value="root"/>
	<!-- specifies the password of the user. -->
	<property name="javax.persistence.jdbc.password" value="root"/>
	<!-- specifies the class name of the JDBC driver to be used. Here we use MySQL Connector Java so the name is com.mysql.jdbc.Driver. -->
	<property name="javax.persistence.jdbc.driver" value="com.mysql.jdbc.Driver"/>
	<!-- tells Hibernate to show SQL statements in standard output. -->
	<property name="hibernate.show_sql" value="true"/>
	<!-- tells Hibernate to format the SQL statements. -->
	<property name="hibernate.format_sql" value="true"/>
</properties>
```
*Remarks: this file has to be under the META-INF folder which has to be created (if not already existing) under the path src/main/resources/. This file basically tells hibernate how to connect to the database.*

When this is done we need to write out some code to communicate with the database.

Here are some useful snippets to perform operations over datebase data:

## Add / Insert

```java
User newUser = new User();
newUser.setEmail("billjoy@gmail.com");
newUser.setFullname("bill Joy");
newUser.setPassword("billi");

entityManager.persist(newUser);
```

## Update

```java
User existingUser = new User();
existingUser.setId(1);
existingUser.setEmail("bill.joy@gmail.com");
existingUser.setFullname("Bill Joy");
existingUser.setPassword("billionaire");

entityManager.merge(existingUser);
```

## Remove

```java
Integer primaryKey = 1;
User reference = entityManager.getReference(User.class, primaryKey);

entityManager.remove(reference);
```

## Find

```java
Integer primaryKey = 1;
User user = entityManager.find(User.class, primaryKey);

System.out.println(user.getEmail());
System.out.println(user.getFullname());
System.out.println(user.getPassword());
```

## Query

```java
String sql = "SELECT u from User u where u.email = 'bill.joy@gmail.com'";
Query query = entityManager.createQuery(sql);
User user = (User) query.getSingleResult();

System.out.println(user.getEmail());
System.out.println(user.getFullname());
System.out.println(user.getPassword());
```

## Tutorial persistence.xml

```xml
<properties>
	<property name="javax.persistence.jdbc.url" value="jdbc:mysql://localhost:3306/catalogdb"/>
	<!-- specifies the username of the account having privilege to access to the database. -->
	<property name="javax.persistence.jdbc.user" value="root"/>
	<!-- specifies the password of the user. -->
	<property name="javax.persistence.jdbc.password" value="root"/>
	<!-- specifies the class name of the JDBC driver to be used. Here we use MySQL Connector Java so the name is com.mysql.jdbc.Driver. -->
	<property name="javax.persistence.jdbc.driver" value="com.mysql.jdbc.Driver"/>
	<!-- tells Hibernate to show SQL statements in standard output. -->
	<property name="hibernate.show_sql" value="true"/>
	<!-- tells Hibernate to format the SQL statements. -->
	<property name="hibernate.format_sql" value="true"/>
</properties>
```

## Extra

Some help to reset the password of a known mysql user:
```sql
SELECT * FROM mysql.user;
UPDATE mysql.user SET Password=PASSWORD('[password]') WHERE User='[username]';
```

mysqld --console --defaults-file="C:\ProgramData\MySQL\MySQL Server 8.0\my.ini" --init-file="C:\Users\Kovox\Downloads\mysql-init.txt"