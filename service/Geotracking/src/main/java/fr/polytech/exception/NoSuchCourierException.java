package fr.polytech.exception;

public class NoSuchCourierException extends RuntimeException {

    public NoSuchCourierException(String message) {
        super(message);
    }
}
