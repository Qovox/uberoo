package fr.polytech.db;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.sql.*;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

@Startup
@Singleton(name = "TrackingPersistence")
public class ConnectionJDBC {

    private static final Logger log = Logger.getLogger(Logger.class.getName());

    // Db config
    private static ResourceBundle bundle = ResourceBundle.getBundle("configDB");
    private static String urlParameters = "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false";
    private static String url = bundle.getString("jdbc.url") + bundle.getString("jdbc.databaseName") + urlParameters;
    private static String user = bundle.getString("jdbc.user");
    private static String password = bundle.getString("jdbc.password");

    // Db connection handling
    private Connection connexion = null;
    private Statement statement = null;

    public ConnectionJDBC() {
        init();
    }

    private void init() {

    }

    private void connect() throws SQLException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException("Cannot find the driver in the classpath!", e);
        }

        connexion = DriverManager.getConnection(url, user, password);
        statement = connexion.createStatement();
    }

    private void close() {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException ignore) {
                log.log(Level.SEVERE, "MySQL Statement failed closing");
            }
        }
        if (connexion != null) {
            try {
                connexion.close();
            } catch (SQLException ignore) {
                log.log(Level.SEVERE, "MySQL Connection failed closing");
            }
        }
    }

    private void close(ResultSet result) {
        if (result != null) {
            try {
                result.close();
            } catch (SQLException ignore) {
                log.log(Level.SEVERE, "MySQL ResultSet failed closing");
            }
        }

        this.close();
    }

    public boolean updateCourierPosition(int courierId, String address) {
        try {
            connect();

            String query = "INSERT IGNORE INTO binding_coursier_position (coursier_id, address)"
                    + " VALUES (?, ?) " +
                    "ON DUPLICATE KEY UPDATE address=?";

            PreparedStatement preparedStmt = connexion.prepareStatement(query);
            preparedStmt.setInt(1, courierId);
            preparedStmt.setString(2, address);
            preparedStmt.setString(3, address);

            preparedStmt.execute();
            return true;
        } catch (SQLException e) {
            log.log(Level.SEVERE, "MySQL Request failed - " + e.getMessage());
            return false;
        } finally {
            close();
        }
    }

    public String getCourierPosition(int orderId) {
        String res = "Not found";
        ResultSet result = null;

        try {
            connect();

            result = statement.executeQuery(
                    "SELECT binding_coursier_position.address, binding_order_coursier.order_id " +
                    "FROM binding_coursier_position " +
                    "INNER JOIN binding_order_coursier " +
                    "ON binding_coursier_position.coursier_id=binding_order_coursier.coursier_id " +
                    "WHERE order_id = " + orderId
            );

            if (result.next()) {
                return result.getString("address");
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, "MySQL Request failed - " + e.getMessage());
        } finally {
            close(result);
        }

        return res;
    }
}
