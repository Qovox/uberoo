package fr.polytech;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public interface GeoTrackingREST {

    @POST
    @Path("/notifyPosition")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    Response notifyPosition(@FormParam("courierId") int courierId, @FormParam("address") String address);

    @GET
    @Path("/trackOrder")
    Response trackOrderToDeliver(@QueryParam("orderId") int orderId);

}
