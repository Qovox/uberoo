package fr.polytech;

import fr.polytech.business.EventType;
import fr.polytech.db.ConnectionJDBC;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.logging.Logger;

@Path("/")
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class GeoTrackingRESTImpl implements GeoTrackingREST {

    private static final Logger log = Logger.getLogger(Logger.class.getName());

    private ConnectionJDBC connectionJDBC = new ConnectionJDBC();

    @EJB
    private JSONHelper helper;

    @GET
    @Produces("text/plain")
    public String help() {
        return "Home page is http://localhost:8080 or http://localhost:8080/geotracking\n" +
                "Possibilities : /notifyPosition, /trackOrder\n";
    }

    @POST
    @Path("/notifyPosition")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Override
    public Response notifyPosition(@FormParam("courierId") int courierId, @FormParam("address") String address) {
        log.info("Courier with id " + courierId + " notified that his position is " + address);

        if(connectionJDBC.updateCourierPosition(courierId, address)){
            KafkaProducerUtils.produceEvent(EventType.COURIER_POSITION.getTopic(), "", helper.prepareMovedMessage(courierId, address));

            return Response.ok("Courier with id " + courierId + " is now at " + address).build();
        }

        return Response.status(Response.Status.NOT_MODIFIED).entity("Courier with id " + courierId + " position can't be registered now").build();
    }

    @GET
    @Path("/trackOrder")
    @Override
    public Response trackOrderToDeliver(@QueryParam("orderId") int orderId) {
        log.info("Customer with order id " + orderId + " wants to check the courier position");

        return Response.ok("Courier in charge of order " + orderId + " is at " + connectionJDBC.getCourierPosition(orderId)).build();
    }

}
