package fr.polytech;

import org.json.JSONObject;

import javax.ejb.Singleton;
import javax.ejb.Startup;

@Startup
@Singleton(name = "JSONHelper")
public class JSONHelper {

    public JSONObject prepareMovedMessage(int courierId, String address){
        JSONObject res = new JSONObject();

        res.put("courierId", courierId);
        res.put("address", address);

        return res;
    }

}
