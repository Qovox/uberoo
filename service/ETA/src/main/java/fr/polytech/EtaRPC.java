package fr.polytech;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService(targetNamespace = "http://informatique.polytech.unice.fr/soa1/ETA/public")
public interface EtaRPC {

    @WebMethod(operationName = "computeTimeToPrepare")
    @WebResult(name = "time")
    int computeTime(@WebParam(name = "idProduct") int id);
}
