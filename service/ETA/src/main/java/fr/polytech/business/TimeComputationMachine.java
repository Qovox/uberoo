package fr.polytech.business;

import java.util.Random;

public class TimeComputationMachine {

    private static final int MIN = 20;
    private static final int MAX = 60;

    public int compute(int id) {
        return new Random().nextInt((MAX - MIN) + 1) + MIN;
    }
}
