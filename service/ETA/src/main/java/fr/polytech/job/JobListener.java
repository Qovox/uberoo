package fr.polytech.job;

import fr.polytech.KafkaConsumerUtils;
import fr.polytech.business.EventType;
import fr.polytech.business.ServiceType;
import fr.polytech.listeners.KafkaEventListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

public class JobListener implements KafkaEventListener {

    private static final Logger log = Logger.getLogger(Logger.class.getName());

    private List<Thread> threads = new ArrayList<>();

    public void startListeners() {
        Thread orderCreatedListener = new Thread(this::startCourierPositionListener);

        orderCreatedListener.start();

        threads.add(orderCreatedListener);
    }

    public void stopListeners() {
        for (Thread thread : threads) {
            thread.interrupt();
        }
    }

    private void startCourierPositionListener() {
        while (true) {
            ConsumerRecords<String, JSONObject> consumerRecords = KafkaConsumerUtils.consumeEvents(Collections.singletonList(EventType.COURIER_POSITION.getTopic()), ServiceType.ETA);
            for (ConsumerRecord<String, JSONObject> consumerRecord : consumerRecords) {
                calculateNewETA(consumerRecord.key(), consumerRecord.value());
            }
        }
    }

    private void calculateNewETA(String key, JSONObject value) {
        log.info("The ETA consumed an event concerning the courier " + value.getInt("courierId") + "position\nThe new calculated ETA is : XXX");
    }
}
