package fr.polytech;

import fr.polytech.business.TimeComputationMachine;

import javax.jws.WebService;

@WebService(targetNamespace = "http://informatique.polytech.unice.fr/soa1/ETA/public",
        portName = "EtaRPCPort",
        serviceName = "EtaRPCService",
        endpointInterface = "fr.polytech.EtaRPC")
public class EtaImpl implements EtaRPC {

    private TimeComputationMachine timeComputationMachine = new TimeComputationMachine();

    @Override
    public int computeTime(int id) {
        return this.timeComputationMachine.compute(id);
    }
}
