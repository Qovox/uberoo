DROP TABLE IF EXISTS categories_items_binding;
DROP TABLE IF EXISTS items;
DROP TABLE IF EXISTS categories;
DROP TABLE IF EXISTS restaurants;
DROP TABLE IF EXISTS promo_codes;
DROP TABLE IF EXISTS composition_items_binding;

CREATE TABLE IF NOT EXISTS restaurants (
	id int(10) NOT NULL,
	`name` varchar(45) NOT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS categories (
	id int(10) NOT NULL,
  `name` varchar(45) NOT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS items (
	id int(10) NOT NULL,
	`name` varchar(45) NOT NULL,
  price float(10) NOT NULL,
  `type` varchar(20) NOT NULL,
  restaurant_id int(10) NOT NULL,
	PRIMARY KEY (id),
  FOREIGN KEY (restaurant_id) REFERENCES restaurants(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS categories_items_binding (
	item_id int(10) NOT NULL,
  category_id int(10) NOT NULL,
	PRIMARY KEY (item_id, category_id),
  FOREIGN KEY (item_id) REFERENCES items(id),
  FOREIGN KEY (category_id) REFERENCES categories(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS promo_codes (
	id varchar(50) NOT NULL,
  `type` varchar(20) NOT NULL,
  discount float(10) NOT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS composition_items_binding (
	item_type varchar(20) NOT NULL,
  promo_code_id varchar(50) NOT NULL,
	PRIMARY KEY (item_type, promo_code_id),
  FOREIGN KEY (promo_code_id) REFERENCES promo_codes(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT IGNORE INTO restaurants (id, `name`) VALUES (1, "Kow Long"); 
INSERT IGNORE INTO restaurants (id, `name`) VALUES (2, "Song Qi"); 
INSERT IGNORE INTO restaurants (id, `name`) VALUES (3, "Ambata"); 
INSERT IGNORE INTO restaurants (id, `name`) VALUES (4, "Luz Verde"); 
INSERT IGNORE INTO restaurants (id, `name`) VALUES (5, "Domani"); 
INSERT IGNORE INTO restaurants (id, `name`) VALUES (6, "L'espelette"); 
INSERT IGNORE INTO restaurants (id, `name`) VALUES (7, "Pizza d'Or"); 
INSERT IGNORE INTO restaurants (id, `name`) VALUES (8, "Arzak"); 
INSERT IGNORE INTO restaurants (id, `name`) VALUES (9, "Sushi Saito");

INSERT IGNORE INTO categories (id, `name`) VALUES (1, "Chinese");
INSERT IGNORE INTO categories (id, `name`) VALUES (2, "Dessert");
INSERT IGNORE INTO categories (id, `name`) VALUES (3, "Mexican");
INSERT IGNORE INTO categories (id, `name`) VALUES (4, "Pizza");
INSERT IGNORE INTO categories (id, `name`) VALUES (5, "Spanish");
INSERT IGNORE INTO categories (id, `name`) VALUES (6, "Spicy");
INSERT IGNORE INTO categories (id, `name`) VALUES (7, "Starter");
INSERT IGNORE INTO categories (id, `name`) VALUES (8, "Sweet_sour");

INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (1, "Kung Pao Chicken", 10.50, "MAIN_COURSE", 1);
INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (2, "Ma Po Tofu", 9.0, "MAIN_COURSE", 1);
INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (3, "Tiramisu", 5.0, "DESSERT", 3);
INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (4, "Floating island", 5.0, "DESSERT", 3);
INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (5, "Chocolate mousse", 5.0, "DESSERT", 9);
INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (6, "Tacos", 6.75, "MAIN_COURSE", 4);
INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (7, "Quesadillas", 6.25, "MAIN_COURSE", 4);
INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (8, "Buldak", 8.0, "MAIN_COURSE", 5);
INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (9, "Abiko Curry", 12.0, "MAIN_COURSE", 5);
INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (10, "Margherita", 11.0, "MAIN_COURSE", 6);
INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (11, "Napoli", 10.75, "MAIN_COURSE", 6);
INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (12, "Napoli", 10.50, "MAIN_COURSE", 7);
INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (13, "Gazpachuelo", 13.10, "MAIN_COURSE", 3);
INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (14, "Paella", 19.80, "MAIN_COURSE", 8);
INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (15, "Watermelon and feta", 12.20, "ENTRY", 3);
INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (16, "Sushi salad", 14.50, "ENTRY", 9);
INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (17, "Sweat and Sour Pork", 15.0, "MAIN_COURSE", 9);

INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (1, 1);
INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (2, 1);
INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (17, 1);

INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (3, 2);
INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (4, 2);
INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (5, 2);

INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (6, 3);
INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (7, 3);

INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (10, 4);
INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (11, 4);
INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (12, 4);

INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (13, 5);
INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (14, 5);

INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (2, 6);
INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (8, 6);
INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (9, 6);

INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (15, 7);
INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (16, 7);

INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (17, 8);

INSERT IGNORE INTO promo_codes (id, `type`, discount) VALUES ("Xmas", "DIRECT", 25.0);
INSERT IGNORE INTO promo_codes (id, `type`, discount) VALUES ("Uberoo", "COMPOSITION", 10.0);

INSERT IGNORE INTO composition_items_binding (item_type, promo_code_id) VALUES ("ENTRY", "Uberoo");
INSERT IGNORE INTO composition_items_binding (item_type, promo_code_id) VALUES ("MAIN_COURSE", "Uberoo");
INSERT IGNORE INTO composition_items_binding (item_type, promo_code_id) VALUES ("DESSERT", "Uberoo");

Select * from categories_items_binding;
Select * from items;
Select * from categories;
Select * from restaurants;
Select * from promo_codes;
Select * from composition_items_binding;
