package fr.polytech.db;

import fr.polytech.business.*;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

@Startup
@Singleton(name = "CatalogDAO")
public class ConnectionJDBC {

    private static final Logger log = Logger.getLogger(Logger.class.getName());

    // Db config
    private static ResourceBundle bundle = ResourceBundle.getBundle("configDB");
    private static String urlParameters = "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false";
    private static String url = bundle.getString("jdbc.url") + bundle.getString("jdbc.databaseName") + urlParameters;
    private static String user = bundle.getString("jdbc.user");
    private static String password = bundle.getString("jdbc.password");

    // Db connection handling
    private Connection connexion = null;
    private Statement statement = null;

    public ConnectionJDBC() {
        init();
    }

    private void init() {

    }

    private void connect() throws SQLException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            System.out.println("Driver loaded!");
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException("Cannot find the driver in the classpath!", e);
        }

        connexion = DriverManager.getConnection(url, user, password);
        statement = connexion.createStatement();
    }

    private void close(ResultSet result) {
        if (result != null) {
            try {
                result.close();
            } catch (SQLException ignore) {
                log.log(Level.SEVERE, "MySQL ResultSet failed closing");
            }
        }
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException ignore) {
                log.log(Level.SEVERE, "MySQL Statement failed closing");
            }
        }
        if (connexion != null) {
            try {
                connexion.close();
            } catch (SQLException ignore) {
                log.log(Level.SEVERE, "MySQL Connection failed closing");
            }
        }
    }

    public List<Category> getCategories() {
        ResultSet result = null;
        List<Category> categories = new ArrayList<>();

        try {
            connect();

            result = statement.executeQuery("SELECT * FROM categories;");

            while (result.next()) {
                String categoryName = result.getString("name");
                Optional<Category> category = getCategoryByName(categoryName);

                if (category.isPresent()) {
                    categories.add(category.get());
                } else {
                    log.log(Level.WARNING, "MySQL Request - couldn't find the category with name '" + categoryName +
                            "'");
                }
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, "MySQL Request failed - " + e.getMessage());
            categories = new ArrayList<>();
        } finally {
            close(result);
        }

        return categories;
    }

    public Optional<Category> getCategoryByName(String name) {
        ResultSet result = null;
        Category category = new Category();

        try {
            connect();

            result = statement.executeQuery("SELECT id, `name` FROM categories WHERE `name`=\"" + name + "\";");
            result.next();

            int id = result.getInt("id");
            String categoryName = result.getString("name");
            category.setId(id);
            category.setName(categoryName);

            /*result = statement.executeQuery("SELECT id, `name`, restaurant_id FROM items WHERE id IN " +
                    "(SELECT item_id FROM categories_items_binding WHERE category_id = " + id + ");");*/
            result = statement.executeQuery("SELECT item_id FROM categories_items_binding WHERE category_id = " + id + ";");
            List<Item> items = new ArrayList<>();

            while (result.next()) {
                int itemId = result.getInt("item_id");
                Optional<Item> item = getItemById(itemId);
                if (item.isPresent()) {
                    items.add(item.get());
                } else {
                    log.log(Level.WARNING, "MySQL Request - couldn't find the item with id '" + itemId +
                            "' bound to the category with id '" + id);
                }
            }

            category.setItems(items);
        } catch (SQLException e) {
            log.log(Level.SEVERE, "MySQL Request failed - " + e.getMessage());
            category = null;
        } finally {
            close(result);
        }

        return category == null ? Optional.empty() : Optional.of(category);
    }

    public Optional<Item> getItemById(int id) {
        ResultSet result = null;
        Item item = new Item();

        try {
            connect();

            result = statement.executeQuery("SELECT id, `name`, `price`, `type`, restaurant_id FROM items WHERE id = " + id + ";");
            result.next();

            int itemId = result.getInt("id");
            String name = result.getString("name");
            float price = result.getFloat("price");
            Optional<ItemType> itemType = ItemType.findByName(result.getString("type"));
            int restaurantId = result.getInt("restaurant_id");
            Optional<Restaurant> restaurant = getRestaurantById(restaurantId);

            item.setId(itemId);
            item.setName(name);
            item.setPrice(price);

            if (itemType.isPresent()) {
                item.setType(itemType.get());
            } else {
                item.setType(ItemType.UNKNOWN);
            }

            if (restaurant.isPresent()) {
                item.setProducer(restaurant.get());
            } else {
                log.log(Level.WARNING, "MySQL Request - couldn't find the restaurant the item with id '" + id + "' is bound to");
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, "MySQL Request failed - " + e.getMessage());
            item = null;
        } finally {
            close(result);
        }

        return item == null ? Optional.empty() : Optional.of(item);
    }

    public Optional<Restaurant> getRestaurantById(int id) {
        ResultSet result = null;
        Restaurant restaurant = new Restaurant();

        try {
            connect();

            result = statement.executeQuery("SELECT id, `name` FROM restaurants WHERE id = " + id + ";");
            result.next();

            int restaurantId = result.getInt("id");
            String name = result.getString("name");
            restaurant.setId(restaurantId);
            restaurant.setName(name);

            result = statement.executeQuery("SELECT id, `name`, `price`, `type` FROM items WHERE restaurant_id = " + id + ";");
            List<Item> items = new ArrayList<>();

            while (result.next()) {
                Optional<ItemType> itemType = ItemType.findByName(result.getString("type"));

                if (itemType.isPresent()) {
                    items.add(new Item(result.getInt("id"), result.getString("name"), result.getFloat("price"), itemType.get(), restaurant));
                } else {
                    items.add(new Item(result.getInt("id"), result.getString("name"), result.getFloat("price"), ItemType.UNKNOWN, restaurant));
                }
            }

            restaurant.setProducts(items);
        } catch (SQLException e) {
            log.log(Level.SEVERE, "MySQL Request failed - " + e.getMessage());
            restaurant = null;
        } finally {
            close(result);
        }

        return restaurant == null ? Optional.empty() : Optional.of(restaurant);
    }

    public Optional<PromoCode> getPromoCodeByName(String name) {
        ResultSet result = null;
        PromoCode promoCode;

        try {
            connect();

            result = statement.executeQuery("SELECT id, `type`, discount FROM promo_codes WHERE `id`=\"" + name + "\";");
            result.next();

            String id = result.getString("id");
            Optional<PromoType> type = PromoType.findByName(result.getString("type"));

            if (type.isPresent()) {
                promoCode = new PromoCode(id, type.get());
                promoCode.setDiscount(result.getFloat("discount"));

                switch (type.get()) {
                    case DIRECT:
                        break;
                    case COMPOSITION:
                        result = statement.executeQuery("SELECT item_type FROM composition_items_binding WHERE promo_code_id=\"" + id + "\";");
                        List<ItemType> itemTypes = new ArrayList<>();

                        while (result.next()) {
                            String itemTypeName = result.getString("item_type");
                            Optional<ItemType> itemType = ItemType.findByName(itemTypeName);

                            if (itemType.isPresent()) {
                                itemTypes.add(itemType.get());
                            } else {
                                promoCode.setPromoType(PromoType.UNKNOWN);
                                itemTypes = new ArrayList<>();
                                log.log(Level.WARNING, "MySQL Request - couldn't find item type '" + itemTypeName);
                                break;
                            }
                        }

                        promoCode.setCompositionItemType(itemTypes);
                    case UNKNOWN:
                    default:
                        log.log(Level.WARNING, "MySQL Request - promo code of type " + ItemType.UNKNOWN.getName());
                        break;
                }
            } else {
                promoCode = new PromoCode(id, PromoType.UNKNOWN);
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, "MySQL Request failed - " + e.getMessage());
            promoCode = null;
        } finally {
            close(result);
        }

        return promoCode == null ? Optional.empty() : Optional.of(promoCode);
    }

    public boolean addPromoCode(PromoCode promoCode) {
        try {
            connect();
            int result;

            result = statement.executeUpdate("INSERT INTO promo_codes (id, `type`, discount) "
                    + "VALUES (\"" + promoCode.getId() + "\", \"" + promoCode.getPromoType().name() + "\", " + promoCode.getDiscount() + ");");

            if (result != 1) {
                log.log(Level.WARNING, "MySQL Request - couldn't add catalogdb. Adding promo code failed.");
                return false;
            }

            if (promoCode.getPromoType() == PromoType.COMPOSITION && promoCode.getComposition() != null && !promoCode.getComposition().isEmpty()) {
                for (ItemType itemType : promoCode.getComposition()) {
                    if (itemType != null) {
                        result = statement.executeUpdate("INSERT INTO composition_items_binding (item_type, promo_code_id) "
                                + "VALUES (\"" + itemType.name() + "\", \"" + promoCode.getId() + "\");");

                        if (result != 1) {
                            log.log(Level.WARNING, "MySQL Request - couldn't update catalogdb. Binding item " + itemType.name() + "to promo code " + promoCode.getId() + " failed.");
                            return false;
                        }
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            close(null);
        }

        return true;
    }
}
