package fr.polytech;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "categories")
public class ListOfCategories {

    private List<String> categories;

    public ListOfCategories() {
        this.categories = new ArrayList<>();
    }

    @XmlElement(name = "category")
    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }
}
