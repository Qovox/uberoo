package fr.polytech;

import fr.polytech.business.Category;
import fr.polytech.business.Item;
import fr.polytech.business.PromoCode;
import fr.polytech.business.PromoType;
import fr.polytech.db.ConnectionJDBC;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Path("/")
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class CatalogRESTImpl implements CatalogREST {

    @EJB
    private ConnectionJDBC dao;

    @GET
    @Produces("text/plain")
    public String help() {
        return "Home page is http://localhost:8080 or http://localhost:8080/catalog\n" +
                "Add or '/items/{itemId}' or '/categories' or '/categories/{categoryId}' to this URL.\n" +
                "Examples:\n" +
                "http://localhost:8080/catalog/items/5\n" +
                "http://localhost:8080/catalog/categories\n" +
                "http://localhost:8080/catalog/categories/Pizza\n" +
                "http://localhost:8080/catalog/promoCodes/Xmas (test only)\n" +
                "http://localhost:8080/catalog/promoCodes/Uberoo (test only)\n" +
                "http://localhost:8080/catalog/promoCodes ->\n" +
                "<promo_code>\n" +
                "<id>Black Friday</id>\n" +
                "<discount>75.0</discount>\n" +
                "<type>DIRECT</type>\n" +
                "</promo_code>\n" +
                "or ->\n" +
                "<promo_code>\n" +
                "<composition>\n" +
                "    <composition>ENTRY</composition>\n" +
                "    <composition>DESSERT</composition>\n" +
                "</composition>\n" +
                "<discount>50.0</discount>\n" +
                "<id>Uberoo Max</id>\n" +
                "<type>COMPOSITION</type>\n" +
                "</promo_code>";
    }

    @GET
    @Path("/items/{itemId}")
    @Override
    public Response readItem(@PathParam("itemId") int itemId) {
        dao = new ConnectionJDBC();
        Optional<Item> item = dao.getItemById(itemId);

        if (!item.isPresent()) {
            UnknownResource error = new UnknownResource("item", String.valueOf(itemId));
            return Response.status(Response.Status.NOT_FOUND).entity(error).build();
        }

        return Response.ok(item.get()).build();
    }

    @GET
    @Path("/categories")
    @Override
    public Response readCategories() {
        List<Category> categories = dao.getCategories();
        List<String> categoriesName = new ArrayList<>();

        if (categories.isEmpty()) {
            UnknownResource error = new UnknownResource("Database unavailable", "categories");
            return Response.status(Response.Status.NOT_FOUND).entity(error).build();
        }

        for (Category c : categories) {
            categoriesName.add(c.getName());
        }

        ListOfCategories result = new ListOfCategories();
        result.setCategories(categoriesName);
        return Response.ok(result).build();
    }

    @GET
    @Path("/categories/{categoryName}")
    @Override
    public Response readCategory(@PathParam("categoryName") String name) {
        Optional<Category> category = dao.getCategoryByName(name);

        if (!category.isPresent()) {
            UnknownResource error = new UnknownResource("category", String.valueOf(name));
            return Response.status(Response.Status.NOT_FOUND).entity(error).build();
        }

        return Response.ok(category.get()).build();
    }

    // Test only.
    @GET
    @Path("/promoCodes/{promoCodeId}")
    @Override
    public Response readPromoCode(@PathParam("promoCodeId") String promoCodeId) {
        Optional<PromoCode> promoCode = dao.getPromoCodeByName(promoCodeId);

        if (!promoCode.isPresent()) {
            UnknownResource error = new UnknownResource("promo_code", String.valueOf(promoCodeId));
            return Response.status(Response.Status.NOT_FOUND).entity(error).build();
        }

        return Response.ok(promoCode.get()).build();
    }

    @POST
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("/promoCodes")
    @Override
    public Response addPromoCode(PromoCode promoCode) {
        // Promo code already existing or not
        if (readPromoCode(promoCode.getId()).getStatus() == 404) {
            // Check minimal requirements
            if (promoCode.getId() == null || promoCode.getDiscount() <= 0 || promoCode.getPromoType() == null) {
                return Response.status(Response.Status.BAD_REQUEST).entity("Bad input: id, discount or type is (are) incorrect").build();
            }

            // Check online payment properties
            if (promoCode.getPromoType() == PromoType.COMPOSITION && (promoCode.getComposition() == null || promoCode.getComposition().isEmpty())) {
                return Response.status(Response.Status.BAD_REQUEST).entity("Bad input: composition promo code has to have at least one composition item type").build();
            }

            boolean result = dao.addPromoCode(promoCode);

            if (!result) {
                return Response.status(Response.Status.BAD_REQUEST).entity("Bad input: Promo code couldn't be added").build();
            }

            return Response.status(Response.Status.CREATED).entity("Promo code successfully added: /promoCodes/" + promoCode.getId()).build();
        } else {
            return Response.status(Response.Status.CONFLICT).entity("Promo code already existing: /promoCodes/" + promoCode.getId()).build();
        }
    }
}
