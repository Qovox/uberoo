package fr.polytech.business;

import java.util.Optional;

public enum ItemType {

    /*CHINESE, MEXICAN, PIZZA, SPANISH, SPICY, STARTER, SWEET_SOUR,*/

    // Promotional code item type
    ENTRY("Entry"), MAIN_COURSE("Main Course"), DESSERT("Dessert"), UNKNOWN("Unknown");

    private String name;

    ItemType(String name) {
        this.name = name;
    }

    public static Optional<ItemType> findByName(String name) {
        for (ItemType i : ItemType.values()) {
            if (i.name().equals(name)) {
                return Optional.of(i);
            }
        }

        return Optional.empty();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
