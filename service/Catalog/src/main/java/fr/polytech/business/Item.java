package fr.polytech.business;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlType
@XmlRootElement(name = "item")
public class Item implements Serializable {

    private int id;
    private String name;
    private float price;
    private ItemType type;
    private Restaurant producer;

    public Item() {
    }

    public Item(int id, String name, float price, ItemType type) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.type = type;
    }

    public Item(int id, String name, float price, ItemType type, Restaurant producer) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.producer = producer;
    }

    public Item(Item that) {  // copy constructor
        this(that.id, that.name, that.price, that.type, that.producer);
    }

    @XmlAttribute(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @XmlElement(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement(name = "price")
    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @XmlElement(name = "type")
    public String getType() {
        return type.getName();
    }

    public void setType(ItemType type) {
        this.type = type;
    }

    @XmlElement(name = "producer")
    public int getProducer() {
        return producer.getId();
    }

    public void setProducer(Restaurant producer) {
        this.producer = producer;
    }

    public Restaurant getRawProducer() {
        return producer;
    }

    @Override
    public boolean equals(Object that) {  // Necessary for object stored in collections ( => find, remove, ...)
        return (this == that || (that instanceof Item && this.id == (((Item) that).id)));
    }

    @Override
    public int hashCode() {
        return String.valueOf(this.id).hashCode();
    }
}
