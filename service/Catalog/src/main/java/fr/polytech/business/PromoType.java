package fr.polytech.business;

import java.util.Optional;

public enum PromoType {

    // Promotional code item type
    DIRECT("Direct"), COMPOSITION("Composition"), UNKNOWN("Unknown");

    private String name;

    PromoType(String name) {
        this.name = name;
    }

    public static Optional<PromoType> findByName(String name) {
        for (PromoType i : PromoType.values()) {
            if (i.name().equals(name)) {
                return Optional.of(i);
            }
        }

        return Optional.empty();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
