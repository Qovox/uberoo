package fr.polytech.business;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlType
@XmlRootElement(name = "promo_code")
public class PromoCode {

    private String id;
    private PromoType type;

    /**
     * Direct promo code specific attribute
     */
    private float discount;

    /**
     * Composition promo code specific attribute
     */
    private List<ItemType> composition;

    public PromoCode() {

    }

    public PromoCode(String id, PromoType type) {
        this.id = id;
        this.type = type;
        discount = -1;
        composition = new ArrayList<>();
    }

    @XmlElement(name = "id")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @XmlElement(name = "type")
    public String getType() {
        return type.getName();
    }

    public void setType(String type) {
        this.type = PromoType.findByName(type).get();
    }

    @XmlTransient
    public PromoType getPromoType() {
        return type;
    }

    public void setPromoType(PromoType type) {
        this.type = type;
    }

    @XmlElement(name = "discount")
    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    /*
    @XmlElementWrapper
    @XmlElement(name = "composition")
    public List<String> getComposition() {
        List<String> itemTypes = new ArrayList<>();

        for (ItemType it : composition) {
            itemTypes.add(it.getName());
        }

        return itemTypes;
    }
    */

    @XmlElementWrapper
    @XmlElement(name = "composition")
    public List<ItemType> getComposition() {
        return composition;
    }

    public void setComposition(List<ItemType> composition) {
        this.composition = composition;
    }

    public void setCompositionItemType(List<ItemType> composition) {
        this.composition = composition;
    }
}
