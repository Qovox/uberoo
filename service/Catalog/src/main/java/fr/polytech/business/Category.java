package fr.polytech.business;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@XmlType
@XmlRootElement(name = "category")
public class Category implements Serializable {

    private int id;
    private String name;
    private List<Item> items = new ArrayList<>();

    public Category() {
    }

    public Category(int id, String name, List<Item> items) {
        this.id = id;
        this.name = name;
        this.items = items;
    }

    @XmlAttribute(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @XmlAttribute(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement(name = "items")
    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    @Override
    public boolean equals(Object that) {  // Necessary for object stored in collections ( => find, remove, ...)
        return (this == that || (that instanceof Category && this.id == ((Category) that).id));
    }

    @Override
    public int hashCode() {
        return String.valueOf(this.id).hashCode();
    }
}
