package fr.polytech.business;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@XmlType
@XmlRootElement(name = "restaurant")
public class Restaurant implements Serializable {

    private int id;
    private String name;
    private List<Item> products;

    public Restaurant() {
    }

    public Restaurant(int id, String name) {
        this.id = id;
        this.name = name;
        this.products = new ArrayList<>();
    }

    @XmlAttribute(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @XmlAttribute(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement(name = "products")
    public List<Item> getProducts() {
        return products;
    }

    public void setProducts(List<Item> products) {
        this.products = products;
    }

    @Override
    public boolean equals(Object that) {  // Necessary for object stored in collections ( => find, remove, ...)
        return (this == that || (that instanceof Restaurant && this.id == ((Restaurant) that).id));
    }

    @Override
    public int hashCode() {
        return String.valueOf(this.id).hashCode();
    }
}
