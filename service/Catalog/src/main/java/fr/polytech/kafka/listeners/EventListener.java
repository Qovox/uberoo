package fr.polytech.kafka.listeners;

import fr.polytech.CatalogRESTImpl;
import fr.polytech.KafkaConsumerUtils;
import fr.polytech.KafkaProducerUtils;
import fr.polytech.business.*;
import fr.polytech.db.ConnectionJDBC;
import fr.polytech.listeners.KafkaEventListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class EventListener implements KafkaEventListener {

    private List<Thread> threads = new ArrayList<>();
    private CatalogRESTImpl catalogREST = new CatalogRESTImpl();
    private ConnectionJDBC dao;

    public void startListeners() {
        Thread confirmCatalogListener = new Thread(() -> {
            startConfirmCatalogListener();
        });
        Thread catalogOrderAmountListener = new Thread(() -> {
            startCatalogOrderAmountListener();
        });
        Thread confirmPromoCodeListener = new Thread(() -> {
            startConfirmPromoCodeListener();
        });

        confirmCatalogListener.start();
        catalogOrderAmountListener.start();
        confirmPromoCodeListener.start();

        threads.add(confirmCatalogListener);
        threads.add(catalogOrderAmountListener);
        threads.add(confirmPromoCodeListener);

        dao = new ConnectionJDBC();
    }

    public void stopListeners() {
        for (Thread thread : threads) {
            thread.interrupt();
        }
    }

    public void startConfirmCatalogListener() {
        while (true) {
            ConsumerRecords<String, JSONObject> consumerRecords = KafkaConsumerUtils.consumeEvents(Arrays.asList(EventType.CONFIRM_CATALOG.getTopic()), ServiceType.CATALOG);
            for (ConsumerRecord<String, JSONObject> consumerRecord : consumerRecords) {
                checkCatalog(consumerRecord.key(), consumerRecord.value());
            }
        }
    }

    public void startCatalogOrderAmountListener() {
        while (true) {
            ConsumerRecords<String, JSONObject> consumerRecords = KafkaConsumerUtils.consumeEvents(Arrays.asList(EventType.GET_ORDER_AMOUNT.getTopic()), ServiceType.CATALOG);
            for (ConsumerRecord<String, JSONObject> consumerRecord : consumerRecords) {
                computeOrderAmount(consumerRecord.key(), consumerRecord.value());
            }
        }
    }

    public void startConfirmPromoCodeListener() {
        while (true) {
            ConsumerRecords<String, JSONObject> consumerRecords = KafkaConsumerUtils.consumeEvents(Arrays.asList(EventType.CONFIRM_PROMO_CODE.getTopic()), ServiceType.CATALOG);
            for (ConsumerRecord<String, JSONObject> consumerRecord : consumerRecords) {
                checkPromoCode(consumerRecord.key(), consumerRecord.value());
            }
        }
    }

    public void checkCatalog(String id, JSONObject json) {
        JSONArray productsIdJsonArray = json.getJSONArray("productIds");

        Response response;
        int uniqueRestaurantId = -1;

        for (int i = 0; i < productsIdJsonArray.length(); ++i) {
            response = catalogREST.readItem(productsIdJsonArray.getInt(i));

            if (response.getStatus() != 200) {
                KafkaProducerUtils.produceEvent(EventType.CATALOG_NOT_OK.getTopic(), id, json);
                return;
            } else {
                int currentRestaurantId = ((Item) response.getEntity()).getRawProducer().getId();

                if (uniqueRestaurantId < 0) {
                    uniqueRestaurantId = currentRestaurantId;
                } else if (uniqueRestaurantId != currentRestaurantId) {
                    KafkaProducerUtils.produceEvent(EventType.CATALOG_NOT_OK.getTopic(), id, json);
                    return;
                }
            }
        }

        json.put("restaurantId", uniqueRestaurantId);
        KafkaProducerUtils.produceEvent(EventType.CATALOG_OK.getTopic(), id, json);
    }

    public void computeOrderAmount(String id, JSONObject json) {
        JSONArray productsIdJsonArray = json.getJSONArray("productIds");

        float amount = 0;

        for (int i = 0; i < productsIdJsonArray.length(); ++i) {
            Optional<Item> item = dao.getItemById(productsIdJsonArray.getInt(i));
            amount += item.get().getPrice();
        }

        json.put("amount", amount);
        KafkaProducerUtils.produceEvent(EventType.GET_ORDER_AMOUNT_RESPONSE.getTopic(), id, json);
    }

    public void checkPromoCode(String id, JSONObject json) {
        String promoCodeId = json.getString("promo_code");

        // Product ids from order.
        JSONArray productsIdJsonArray = json.getJSONArray("product_ids");
        Optional<PromoCode> promoCode = dao.getPromoCodeByName(promoCodeId);

        if (promoCode.isPresent()) {
            check:
            switch (promoCode.get().getPromoType()) {
                case DIRECT:
                    json.put("discount", promoCode.get().getDiscount());
                    json.put("promo_code_response", "matching");
                    break;
                case COMPOSITION:
                    List<ItemType> composition = new ArrayList<>(promoCode.get().getComposition());

                    for (int i = 0; i < productsIdJsonArray.length(); ++i) {
                        Item item = (Item) catalogREST.readItem(productsIdJsonArray.getInt(i)).getEntity();

                        for (ItemType it : composition) {
                            if (it.getName().equals(item.getType())) {
                                composition.remove(it);
                                break;
                            }
                        }

                        if (composition.isEmpty()) {
                            json.put("discount", promoCode.get().getDiscount());
                            json.put("promo_code_response", "matching");
                            break check;
                        }
                    }

                    json.put("promo_code_response", "no_matching");
                    break;
                case UNKNOWN:
                default:
                    json.put("promo_code_response", "not_existing");
            }
        } else {
            json.put("promo_code_response", "not_existing");
        }

        KafkaProducerUtils.produceEvent(EventType.CONFIRM_PROMO_CODE_RESPONSE.getTopic(), id, json);
    }
}
