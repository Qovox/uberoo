package fr.polytech;

import fr.polytech.business.PromoCode;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public interface CatalogREST {

    @GET
    @Path("/items/{itemId}")
    Response readItem(@PathParam("itemId") int itemId);

    @GET
    @Path("/categories")
    Response readCategories();

    @GET
    @Path("/categories/{categoryName}")
    Response readCategory(@PathParam("categoryName") String name);

    // Test only.
    @GET
    @Path("/promoCodes/{promoCodeId}")
    Response readPromoCode(@PathParam("promoCodeId") String promoCodeId);

    @POST
    @Path("/promoCodes")
    Response addPromoCode(PromoCode promoCode);
}
