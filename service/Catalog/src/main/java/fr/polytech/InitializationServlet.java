package fr.polytech;

import fr.polytech.kafka.listeners.EventListener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class InitializationServlet implements ServletContextListener {
    private EventListener eventListener = new EventListener();

    @Override
    public void contextInitialized(ServletContextEvent contextEvent) {
        eventListener.startListeners();
    }

    @Override
    public void contextDestroyed(ServletContextEvent contextEvent) {
        eventListener.stopListeners();
    }
}
