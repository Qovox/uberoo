package fr.polytech.listeners;

public interface KafkaEventListener {

    void startListeners();

    void stopListeners();

}
