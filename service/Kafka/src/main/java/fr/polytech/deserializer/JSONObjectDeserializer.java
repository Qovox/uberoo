package fr.polytech.deserializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Deserializer;
import org.json.JSONObject;

import java.util.Map;

public class JSONObjectDeserializer implements Deserializer {

    @Override
    public void configure(Map configs, boolean isKey) {

    }

    @Override
    public Object deserialize(String topic, byte[] bytes) {
        ObjectMapper mapper = new ObjectMapper();
        String jsonObjectString = null;
        try {
            jsonObjectString = mapper.readValue(bytes, String.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONObject jsonObject = new JSONObject(jsonObjectString);
        return jsonObject;
    }

    @Override
    public void close() {

    }
}
