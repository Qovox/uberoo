package fr.polytech.business;

import javax.xml.bind.annotation.XmlElement;

public class OrderEvent {

    private int orderId;

    private int productId;

    public OrderEvent(int orderId, int productId) {
        this.productId = productId;
        this.orderId = orderId;
    }

    public OrderEvent() {
    }

    @XmlElement(name = "orderId", required = true)
    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    @XmlElement(name = "productId", required = true)
    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }
}
