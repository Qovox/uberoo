package fr.polytech.business;

public enum EventType {

    /* Sent by Catalog */
    CATALOG_OK("Catalog_OK"),
    CATALOG_NOT_OK("Catalog_Not_OK"),
    CONFIRM_PROMO_CODE_RESPONSE("Confirm_Promo_Code_Response"),
    GET_ORDER_AMOUNT_RESPONSE("Get_Order_Amount_Response"),

    /* Sent by Courier */
    ORDER_PICKED("Order_Picked"),
    ORDER_DELIVERED("Order_Delivered"),

    /* Sent by GeoTracking */
    COURIER_POSITION("Courier_Position"),

    /* Sent by Order */
    ORDER_CREATED("Order_Created"),
    ORDER_CANCELED("Order_Canceled"),
    CONFIRM_CATALOG("Confirm_Catalog"),
    ORDER_PREPARED("Order_Prepared"),
    DATABASE_PROBLEM("Database_Problem"),
    REVIEW_SENT("Review_Sent"),

    /* Sent by Payment */
    GET_ORDER_AMOUNT("Get_Order_Amount"),
    CONFIRM_PROMO_CODE("Confirm_Promo_Code"),
    PAYMENT_CHOSEN("Payment_Chosen");

    /* Sent by Statistics */

    private final String topic;

    EventType(final String topic) {
        this.topic = topic;
    }

    public String getTopic() {
        return this.topic;
    }
}
