package fr.polytech.business;

public enum ServiceType {

    ORDER(0),
    CATALOG(1),
    ETA(2),
    STATISTIC(3),
    PAYMENT(4),
    COURIER(5);

    private final Integer value;

    ServiceType(final Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return this.value;
    }
}
