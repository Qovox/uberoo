package fr.polytech;

import fr.polytech.business.ServiceType;
import fr.polytech.deserializer.JSONObjectDeserializer;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

public class KafkaConsumerUtils {

    public static KafkaConsumer<String, JSONObject> createKafkaConsumer(String groupId) {
        Properties props = new Properties();
        props.put("bootstrap.servers", "kafka:9092, 127.0.0.1:9092");
        props.put("group.id", groupId);
        props.put("enable.auto.commit", "false");
        props.put("auto.commit.interval.ms", "1000");
        props.put("auto.offset.reset", "latest");
        props.put("key.deserializer", StringDeserializer.class);
        props.put("value.deserializer", JSONObjectDeserializer.class);

        return new KafkaConsumer<>(props);
    }

    public static void display(ConsumerRecord<Integer, String> record) {
        System.out.println(String.format("key = %s, topic = %s, partition: %d, offset: %d: %s", record.key(), record.topic(), record.partition(), record.offset(), record.value()));
    }

    public static void manualAsynchronousCommit(KafkaConsumer<?, ?> consumer) {
        consumer.commitAsync((offsets, exception) ->
        {
            if (exception != null) {
                exception.printStackTrace();
            } else if (offsets != null) {
                offsets.forEach((topicPartition, offsetAndMetadata) -> {
                    // System.out.printf("commited offset %d for partition %d of topic %s%n",
                    //        offsetAndMetadata.offset(), topicPartition.partition(), topicPartition.topic());
                });
            }
        });
    }

    public static ConsumerRecord<String, JSONObject> consumeEventWithSpecificGroupId(List<String> eventTypeList, String id) {
        do {
            ConsumerRecords<String, JSONObject> records = consumeEvents(eventTypeList, id);
            for(ConsumerRecord<String, JSONObject> record: records) {
                if(record.key().equals(id)) {
                    return record;
                }
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } while (true);
    }

    public static ConsumerRecords<String, JSONObject> consumeEvents(List<String> eventTypeList, String groupId) {
        KafkaConsumer<String, JSONObject> consumer = createKafkaConsumer(groupId);
        List<TopicPartition> eventTypeListWithCluster = new ArrayList<>();
        for (String eventType : eventTypeList) {
            eventTypeListWithCluster.add(new TopicPartition(eventType, 0));
        }
        consumer.assign(eventTypeListWithCluster);

        ConsumerRecords<String, JSONObject> consumerRecords = consumer.poll(10);
        manualAsynchronousCommit(consumer);
        consumer.close();
        return consumerRecords;
    }

    public static ConsumerRecords<String, JSONObject> consumeEvents(List<String> eventTypeList, ServiceType serviceType) {
        return consumeEvents(eventTypeList, serviceType.getValue().toString());
    }

    public static class ConsumerRebalance implements ConsumerRebalanceListener {
        public void onPartitionsAssigned(Collection<TopicPartition> partitions) {
            partitions.forEach(
                    topicPartition ->
                            System.out.printf("Assigned partition %d of topic %s%n",
                                    topicPartition.partition(), topicPartition.topic())
            );

        }

        public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
            for (TopicPartition topicPartition : partitions) {
                System.out.printf("Revoked partition %d of topic %s%n",
                        topicPartition.partition(), topicPartition.topic());
            }

        }
    }

}
