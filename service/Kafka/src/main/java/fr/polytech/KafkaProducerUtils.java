package fr.polytech;

import fr.polytech.serializers.JSONObjectSerializer;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class KafkaProducerUtils {

    // Create Kafka producer with parameters
    private static KafkaProducer<String, JSONObject> createKafkaProducer() {
        Map<String, Object> map = new HashMap<>();
        map.put("bootstrap.servers", "kafka:9092, 127.0.0.1:9092");
        map.put("key.serializer", StringSerializer.class);
        map.put("value.serializer", JSONObjectSerializer.class);

        return new org.apache.kafka.clients.producer.KafkaProducer<>(map);
    }

    // Send message to Topic in Kafka
    private static <T, U> void SendToKafka(org.apache.kafka.clients.producer.KafkaProducer<T, U> producer, ProducerRecord<T, U> record) {
        try {
            producer.send(record, new ProducerCallback());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void produceEvent(String eventType, String key, JSONObject value) {

        KafkaProducer<String, JSONObject> producer = createKafkaProducer();
        ProducerRecord<String, JSONObject> record = new ProducerRecord<>(eventType, key, value);
        SendToKafka(producer, record);
        producer.flush();
        producer.close();
    }

    // Method to see if the message is send, if no exception the message is send
    private static class ProducerCallback implements Callback {
        @Override
        public void onCompletion(RecordMetadata recordMetadata, Exception e) {
            if (e != null) {
                e.printStackTrace();
            } else {
                System.out.println("sent on : " + recordMetadata.topic() + " offset : " + recordMetadata.offset());
            }

        }
    }
}
