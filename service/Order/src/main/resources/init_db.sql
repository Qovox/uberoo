DROP TABLE IF EXISTS products_ordered;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS reviews;

Create table orders(
  id INT(11) not null AUTO_INCREMENT,
  date_creation VARCHAR(50) not null,
  restaurantId INT(11) not null,
  state VARCHAR(20) not null,
  customerName VARCHAR(30) not null,
  adress VARCHAR(70) not null,
  primary key (id)
  ) ENGINE = INNODB;

Create table products_ordered(
  id INT(11) not null,
  product_id INT(11) not null,
  primary key (id, product_id),
  FOREIGN KEY(id) REFERENCES orders(id)
  ) ENGINE = INNODB;

Create table reviews(
  id INT(11) not null,
  restaurantId INT(11) not null,
  customerName VARCHAR(30) not null,
  review VARCHAR(300) not null,
  primary key (id, restaurantId, customerName)
  ) ENGINE = INNODB;

insert into orders (date_creation, restaurantId, state, customerName, adress) VALUES ('2018-10-07 10:45:32', 2, 'ORDERED', 'Seb', '5 rue Michel 06560 Valbonne');
insert into orders (date_creation, restaurantId, state, customerName, adress) VALUES ('2018-10-07 14:12:27', 4, 'COOKING', 'Alice', 'Lille');
insert into orders (date_creation, restaurantId, state, customerName, adress) VALUES ('2018-10-07 17:32:57', 1, 'ORDERED', 'John', 'Place Republique');

insert into products_ordered (id, product_id) VALUES (1, 5);
insert into products_ordered (id, product_id) VALUES (2, 4);
insert into products_ordered (id, product_id) VALUES (2, 6);
insert into products_ordered (id, product_id) VALUES (3, 3);

insert into reviews (id, restaurantId, customerName, review) VALUES (2, 4, 'Alice', 'good meal !');

Select * from orders;
Select * from products_ordered;
Select * from reviews;