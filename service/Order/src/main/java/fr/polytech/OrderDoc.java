package fr.polytech;

import fr.polytech.inputs.BadJobInput;
import fr.polytech.inputs.DataBaseException;
import fr.polytech.inputs.JobInput;
import fr.polytech.outputs.JobResult;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService(targetNamespace = "http://informatique.polytech.unice.fr/soa1/order/public")
public interface OrderDoc {

    @WebMethod(operationName = "process")
    @WebResult(name = "result")
    public JobResult dispatch(@WebParam(name = "event") JobInput input) throws BadJobInput, DataBaseException;
}
