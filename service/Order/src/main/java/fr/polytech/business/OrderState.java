package fr.polytech.business;

public enum OrderState {
    ORDERED,
    PAYMENT_CHOSEN,
    COOKING,
    PREPARED,
    DELIVERED
}
