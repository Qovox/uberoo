package fr.polytech.business;

import org.json.JSONArray;
import org.json.JSONObject;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

@XmlType
@XmlRootElement(name = "order")
public class Order {

    private String customerName;

    private List<Integer> productIds;

    private int restaurantId;

    private OrderState state;

    private int idOrder;

    private String address;

    private String date;

    public Order() {
        updateState(OrderState.ORDERED);
    }

    public Order(String customerName, List<Integer> productIds, OrderState orderState, int restaurantId, int idOrder, String address, String date) {
        this.customerName = customerName;
        this.productIds = productIds;
        this.state = orderState;
        this.restaurantId = restaurantId;
        this.idOrder = idOrder;
        this.address = address;
        this.date = date;
    }

    public Order(JSONObject orderJsonObject) {
        if (orderJsonObject.has("customerName"))
            this.customerName = orderJsonObject.getString("customerName");
        if (orderJsonObject.has("restaurantId"))
            this.restaurantId = orderJsonObject.getInt("restaurantId");
        if (orderJsonObject.has("orderState"))
            this.state = OrderState.valueOf(orderJsonObject.getString("orderState"));
        if (orderJsonObject.has("address"))
            this.address = orderJsonObject.getString("address");
        if (orderJsonObject.has("date"))
            this.date = orderJsonObject.getString("date");
        if (orderJsonObject.has("orderId"))
            this.idOrder = orderJsonObject.getInt("orderId");
        if (orderJsonObject.has("productIds")) {
            JSONArray productsJsonArray = orderJsonObject.getJSONArray("productIds");
            List<Integer> productIds = new ArrayList<>();
            for (int i = 0; i < productsJsonArray.length(); i++)
                productIds.add(productsJsonArray.getInt(i));
            this.productIds = productIds;
        }
    }

    public void updateState(OrderState orderState) {
        this.state = orderState;
    }

    public void specifyOrderId(int id) {
        this.idOrder = id;
    }

    @XmlElement(name = "customerName", required = true)
    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    @XmlElementWrapper
    @XmlElement(name = "productId", required = true)
    public List<Integer> getProductIds() {
        return productIds;
    }

    public void setProductIds(List<Integer> productIds) {
        this.productIds = productIds;
    }

    @XmlElement(name = "restaurantId", required = true)
    public int getRestaurantId() {
        return restaurantId;
    }

    public void specifyRestaurantId(int restaurantId) {
        this.restaurantId = restaurantId;
    }

    @XmlElement(name = "state", required = true)
    public OrderState getState() {
        return state;
    }

    @XmlElement(name = "orderId", required = true)
    public int getIdOrder() {
        return idOrder;
    }

    @XmlElement(name = "adress", required = true)
    public String getAdress() {
        return address;
    }

    public void setAdress(String adress) {
        this.address = adress;
    }

    @XmlElement(name = "date", required = true)
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public JSONObject toJsonObject() {
        JSONObject orderJsonObject = new JSONObject();
        orderJsonObject.put("customerName", this.customerName);
        orderJsonObject.put("productIds", this.productIds);
        orderJsonObject.put("orderState", this.state.toString());
        orderJsonObject.put("address", this.address);
        orderJsonObject.put("date", this.date);
        orderJsonObject.put("restaurantId", this.restaurantId);

        return orderJsonObject;
    }
}
