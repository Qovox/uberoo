package fr.polytech.business;

import org.json.JSONObject;

import javax.xml.bind.annotation.XmlElement;

public class Review {

    private String customerName;
    private int orderId;
    private int restaurantId;
    private String review;

    public Review(){}

    public Review(JSONObject jsonObject) {
        if (jsonObject.has("customerName"))
            this.customerName = jsonObject.getString("customerName");
        if (jsonObject.has("restaurantId"))
            this.restaurantId = jsonObject.getInt("restaurantId");
        if (jsonObject.has("orderId"))
            this.orderId = jsonObject.getInt("orderId");
        if (jsonObject.has("review"))
            this.review = jsonObject.getString("review");
    }

    public Review(String customerName, int orderId, int restaurantId, String review) {
        this.customerName = customerName;
        this.orderId = orderId;
        this.restaurantId = restaurantId;
        this.review = review;
    }

    @XmlElement(name = "customerName", required = true)
    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    @XmlElement(name = "orderId", required = true)
    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    @XmlElement(name = "restaurantId", required = true)
    public int getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(int restaurantId) {
        this.restaurantId = restaurantId;
    }

    @XmlElement(name = "review", required = true)
    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public JSONObject toJsonObject() {
        JSONObject orderJsonObject = new JSONObject();
        orderJsonObject.put("customerName", this.customerName);
        orderJsonObject.put("orderId", this.orderId);
        orderJsonObject.put("restaurantId", this.restaurantId);
        orderJsonObject.put("review", this.review);

        return orderJsonObject;
    }
}
