package fr.polytech.job;

import fr.polytech.KafkaConsumerUtils;
import fr.polytech.KafkaProducerUtils;
import fr.polytech.business.ServiceType;
import fr.polytech.db.ConnectionJDBC;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.Arrays;

import static fr.polytech.business.EventType.CATALOG_NOT_OK;
import static fr.polytech.business.EventType.DATABASE_PROBLEM;
import static fr.polytech.business.EventType.ORDER_CANCELED;

public class CatalogNotOkListener implements Runnable {
    @Override
    public void run() {
        while(true) {
            ConsumerRecords<String, JSONObject> consumerRecords = KafkaConsumerUtils.consumeEvents(Arrays.asList(CATALOG_NOT_OK.getTopic()), ServiceType.ORDER);
            for(ConsumerRecord<String, JSONObject> consumerRecord : consumerRecords){
                catalogNotOk(consumerRecord.key(), consumerRecord.value());
            }
            try {
                Thread.sleep(10);
            }catch (Exception e){e.printStackTrace();}
        }
    }

    public void catalogNotOk(String id, JSONObject orderJsonObject){
        KafkaProducerUtils.produceEvent(ORDER_CANCELED.getTopic(), id, orderJsonObject);
    }
}
