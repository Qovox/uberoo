package fr.polytech.job;

import fr.polytech.listeners.KafkaEventListener;
import java.util.ArrayList;
import java.util.List;

public class JobListener  implements KafkaEventListener {

    List<Thread> threads = new ArrayList<>();

    public void startListeners() {
        Thread catalogOkListener = new Thread(new CatalogOkListener());
        Thread catalogNotOKListener = new Thread(new CatalogNotOkListener());
        Thread orderDeliveredListener = new Thread(new OrderDeliveredListener());
        Thread paymentSelectedListener = new Thread(new PaymentSelectedListener());

        catalogOkListener.start();
        catalogNotOKListener.start();
        orderDeliveredListener.start();
        paymentSelectedListener.start();
        threads.add(catalogOkListener);

        threads.add(catalogNotOKListener);
        threads.add(orderDeliveredListener);
        threads.add(paymentSelectedListener);
    }

    public void stopListeners(){
        for(Thread thread : threads){
            thread.interrupt();
        }
    }
}
