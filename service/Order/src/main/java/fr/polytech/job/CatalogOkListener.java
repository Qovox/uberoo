package fr.polytech.job;

import fr.polytech.KafkaConsumerUtils;
import fr.polytech.KafkaProducerUtils;
import fr.polytech.business.Order;
import fr.polytech.business.ServiceType;
import fr.polytech.db.ConnectionJDBC;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.Arrays;

import static fr.polytech.business.EventType.CATALOG_OK;
import static fr.polytech.business.EventType.DATABASE_PROBLEM;
import static fr.polytech.business.EventType.ORDER_CREATED;

public class CatalogOkListener implements Runnable {
    @Override
    public void run() {
        while(true) {
            ConsumerRecords<String, JSONObject> consumerRecords = KafkaConsumerUtils.consumeEvents(Arrays.asList(CATALOG_OK.getTopic()), ServiceType.ORDER);
            for(ConsumerRecord<String, JSONObject> consumerRecord : consumerRecords){
                catalogOk(consumerRecord.key(), consumerRecord.value());
            }
            try {
                Thread.sleep(10);
            }catch (Exception e){e.printStackTrace();}
        }
    }

    public void catalogOk(String id, JSONObject orderJsonObject){
        try {
            int idGenerated = ConnectionJDBC.addOrder(new Order(orderJsonObject));
            orderJsonObject.put("orderId", idGenerated);
            KafkaProducerUtils.produceEvent(ORDER_CREATED.getTopic(), id, orderJsonObject);
        } catch (SQLException e) {
            KafkaProducerUtils.produceEvent(DATABASE_PROBLEM.getTopic(), id, orderJsonObject);
        }
    }
}
