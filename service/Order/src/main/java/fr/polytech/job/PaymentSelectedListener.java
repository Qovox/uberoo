package fr.polytech.job;

import fr.polytech.KafkaConsumerUtils;
import fr.polytech.business.OrderState;
import fr.polytech.business.ServiceType;
import fr.polytech.db.ConnectionJDBC;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.Arrays;

import static fr.polytech.business.EventType.PAYMENT_CHOSEN;

public class PaymentSelectedListener  implements Runnable{

    @Override
    public void run() {
        while(true) {
            ConsumerRecords<String, JSONObject> consumerRecords = KafkaConsumerUtils.consumeEvents(Arrays.asList(PAYMENT_CHOSEN.getTopic()), ServiceType.ORDER);
            for(ConsumerRecord<String, JSONObject> consumerRecord : consumerRecords){
                updateState(consumerRecord.value());
            }
            try {
                Thread.sleep(10);
            }catch (Exception e){e.printStackTrace();}
        }
    }

    public void updateState(JSONObject updateStateJsonObject){
        try{
            ConnectionJDBC.updateState(updateStateJsonObject.getInt("id"), OrderState.PAYMENT_CHOSEN.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
