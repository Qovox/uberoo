package fr.polytech;

import fr.polytech.job.JobListener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class InitializationServlet implements ServletContextListener {
    JobListener jobListener = new JobListener();

    @Override
    public void contextInitialized(ServletContextEvent contextEvent) {
        jobListener.startListeners();
    }

    @Override
    public void contextDestroyed(ServletContextEvent contextEvent) {
        jobListener.stopListeners();
    }
}
