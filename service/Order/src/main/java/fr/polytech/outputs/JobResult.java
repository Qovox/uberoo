package fr.polytech.outputs;

import javax.xml.bind.annotation.XmlSeeAlso;

@XmlSeeAlso({MessageOutput.class, ListOrder.class, OrderOutput.class, ListReviews.class})
public abstract class JobResult {
}
