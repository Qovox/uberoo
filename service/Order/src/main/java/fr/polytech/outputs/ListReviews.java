package fr.polytech.outputs;

import fr.polytech.business.Review;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.ArrayList;
import java.util.List;

public class ListReviews extends JobResult {

    private List<Review> reviews;

    public ListReviews() {
        reviews = new ArrayList<>();
    }

    @XmlElementWrapper(name = "ListReviews")
    @XmlElement(name = "Review")
    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }
}
