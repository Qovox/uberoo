package fr.polytech.outputs;

import fr.polytech.business.Order;

import javax.xml.bind.annotation.XmlElement;

public class OrderOutput extends JobResult {

    private Order order;

    @XmlElement(name = "order")
    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
}
