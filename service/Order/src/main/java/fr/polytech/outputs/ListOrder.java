package fr.polytech.outputs;

import fr.polytech.business.Order;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.ArrayList;
import java.util.List;

public class ListOrder extends JobResult {

    private List<Order> orders;

    public ListOrder() {
        orders = new ArrayList<>();
    }

    @XmlElementWrapper(name = "ListOrder")
    @XmlElement(name = "Order")
    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }
}
