package fr.polytech.db;

import fr.polytech.business.Order;
import fr.polytech.business.OrderState;
import fr.polytech.business.Review;
import org.json.JSONObject;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import static fr.polytech.business.OrderState.*;

public class ConnectionJDBC {

    private static final Logger log = Logger.getLogger(Logger.class.getName());
    private static ResourceBundle bundle = ResourceBundle.getBundle("configDB");
    // mysql db user
    private static final String user = bundle.getString("jdbc.user");
    // mysql db password
    private static final String password = bundle.getString("jdbc.password");
    // mysql db url (need to specify TimeZone as parameter
    private static String urlParameters = "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false";
    private static final String url = bundle.getString("jdbc.url") + bundle.getString("jdbc.databaseName") + urlParameters;
    private static Connection connexion = null;
    private static Statement statement = null;
    private static Statement statement_product = null;
    private static ResultSet result = null;
    private static ResultSet result_product = null;

    public static Optional<Order> getOrdersById(int id) throws SQLException {
        Optional<Order> orderOptional = Optional.empty();

        try {
            connect();
            // query
            result = statement.executeQuery("SELECT id, restaurantId, state, customerName, adress, date_creation FROM orders WHERE id = " + id + ";");

            // start iterator over the results
            if (result.next()) {

                int idOrder = result.getInt("id");
                int restaurantId = result.getInt("restaurantId");
                String state = result.getString("state");
                String customerName = result.getString("customerName");
                String adress = result.getString("adress");
                String date = result.getString("date_creation");

                result_product = statement_product.executeQuery("SELECT id, product_id FROM products_ordered WHERE id = " + idOrder + ";");
                List<Integer> productIds = new ArrayList<>();
                while (result_product.next()) {
                    productIds.add(result_product.getInt("product_id"));
                }

                orderOptional = Optional.of(new Order(customerName, productIds, OrderState.valueOf(state), restaurantId, idOrder, adress, date));
            }

        } finally {
            close();
        }
        return orderOptional;
    }

    public static List<Order> getOrdersByRestaurantId(int id) throws SQLException {
        List<Order> orders = new ArrayList<>();

        try {
            connect();

            result = statement.executeQuery("SELECT id, restaurantId, state, customerName, adress, date_creation FROM orders WHERE restaurantId = " + id + " AND state IN ('" + PAYMENT_CHOSEN + "', '" + COOKING + "');");

            while (result.next()) {
                int idOrder = result.getInt("id");
                int restaurantId = result.getInt("restaurantId");
                String state = result.getString("state");
                String customerName = result.getString("customerName");
                String adress = result.getString("adress");
                String date = result.getString("date_creation");

                result_product = statement_product.executeQuery("SELECT id, product_id FROM products_ordered WHERE id = " + idOrder + ";");
                List<Integer> productIds = new ArrayList<>();
                while (result_product.next()) {
                    productIds.add(result_product.getInt("product_id"));
                }

                Order order = new Order(customerName, productIds, OrderState.valueOf(state), restaurantId, idOrder, adress, date);
                orders.add(order);
            }

        } finally {
            close();
        }
        return orders;
    }

    public static List<Review> getReviewsByRestaurantId(int id) throws SQLException {
        List<Review> reviews = new ArrayList<>();

        try {
            connect();

            result = statement.executeQuery("SELECT id, restaurantId, customerName, review FROM reviews WHERE restaurantId = " + id + ";");

            while (result.next()) {
                int idOrder = result.getInt("id");
                int restaurantId = result.getInt("restaurantId");
                String customerName = result.getString("customerName");
                String reviewText = result.getString("review");

                Review review = new Review(customerName, idOrder, restaurantId, reviewText);
                reviews.add(review);
            }

        } finally {
            close();
        }
        return reviews;
    }

    public static int addOrder(Order order) throws SQLException {
        String customerName = order.getCustomerName();
        List<Integer> productIds = order.getProductIds();
        int restaurantId = order.getRestaurantId();
        String state = order.getState().toString();
        String adress = order.getAdress();
        String date = order.getDate();

        int generatedId;

        try {
            connect();

            statement.executeUpdate("INSERT INTO orders (restaurantId, state, customerName, date_creation, adress) "
                    + "VALUES (" + restaurantId + ", '" + state + "', '" + customerName + "', '" + date + "', '" + adress + "');", Statement.RETURN_GENERATED_KEYS);

            result = statement.getGeneratedKeys();
            result.next();
            generatedId = result.getInt(1);

            for (int productId : productIds) {
                statement_product.executeUpdate("INSERT INTO products_ordered (id, product_id) "
                        + "VALUES (" + generatedId + ", " + productId + ");");
            }

        } finally {
            close();
        }
        return generatedId;
    }

    public static void addReview(Review review) throws SQLException{
        try {
            connect();

            statement.executeUpdate("INSERT INTO reviews (id, restaurantId, customerName, review) "
                    + "VALUES (" + review.getOrderId() + ", " + review.getRestaurantId() + ", '" + review.getCustomerName() + "', '" + review.getReview() + "');");
        } finally {
            close();
        }
    }


    public static boolean updateState(int id, String state) throws SQLException {

        try {
            connect();

            statement.executeUpdate("UPDATE orders SET state = '" + state + "' WHERE id = " + id);

        } finally {
            close();
        }
        return true;
    }

    public static boolean updateRestaurantId(int id, int restaurantId) throws SQLException {

        try {
            connect();

            statement.executeUpdate("UPDATE orders SET restaurantId = '" + restaurantId + "' WHERE id = " + id);

        } finally {
            close();
        }
        return true;
    }

    public static boolean hasAlreadyOrder(Order order) throws SQLException {
        boolean exist = false;
        try {
            connect();
            // query
            result = statement.executeQuery("SELECT date_creation FROM orders WHERE customerName = '" + order.getCustomerName() + "' AND date_creation = '" + order.getDate() + "' AND adress = '" + order.getAdress() + "';");

            // start iterator over the results
            if (result.next()) {

                exist = true;
            }

        } finally {
            close();
        }
        return exist;
    }

    public static boolean deleteOrder(int id) throws SQLException {

        try {
            connect();

            statement_product.executeUpdate("DELETE FROM products_ordered WHERE id = " + id);
            statement.executeUpdate("DELETE FROM orders WHERE id = " + id);

        } finally {
            close();
        }
        return true;
    }

    private static void connect() throws SQLException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException("Cannot find the driver in the classpath!", e);
        }
        // specify connection to the driver
        connexion = DriverManager.getConnection(url, user, password);
        statement = connexion.createStatement();
        statement_product = connexion.createStatement();
    }

    private static void close() {
        if (result != null) {
            try {
                result.close();
            } catch (SQLException ignore) {
                log.log(Level.SEVERE, "MySQL ResultSet failed closing");
            }
        }
        if (result_product != null) {
            try {
                result_product.close();
            } catch (SQLException ignore) {
                log.log(Level.SEVERE, "MySQL ResultSet failed closing");
            }
        }
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException ignore) {
                log.log(Level.SEVERE, "MySQL Statement failed closing");
            }
        }
        if (statement_product != null) {
            try {
                statement_product.close();
            } catch (SQLException ignore) {
                log.log(Level.SEVERE, "MySQL Statement failed closing");
            }
        }
        if (connexion != null) {
            try {
                connexion.close();
            } catch (SQLException ignore) {
                log.log(Level.SEVERE, "MySQL Connection failed closing");
            }
        }
    }
}
