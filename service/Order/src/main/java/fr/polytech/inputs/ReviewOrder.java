package fr.polytech.inputs;

import fr.polytech.KafkaProducerUtils;
import fr.polytech.business.Review;
import fr.polytech.db.ConnectionJDBC;
import fr.polytech.outputs.JobResult;
import fr.polytech.outputs.MessageOutput;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.sql.SQLException;

import static fr.polytech.business.EventType.REVIEW_SENT;

@XmlType(name = "review_order")
public class ReviewOrder extends JobInput {

    @XmlElement(name = "myReview", required = true)
    private Review myReview;

    @Override
    protected JobResult run() throws BadJobInput, DataBaseException {
        try {
            ConnectionJDBC.addReview(myReview);
            KafkaProducerUtils.produceEvent(REVIEW_SENT.getTopic(), "", myReview.toJsonObject());
            MessageOutput result = new MessageOutput();
            result.setMessage("review sent");
            return result;
        } catch (SQLException e) {
            throw new DataBaseException("Service not available (database problem)");
        }
    }

    @Override
    protected void check() throws BadJobInput {
        if (myReview == null || myReview.getReview() == null || myReview.getCustomerName() == null || myReview.getRestaurantId() == 0 || myReview.getOrderId() == 0) {
            throw new BadJobInput("invalid review");
        }
    }
}
