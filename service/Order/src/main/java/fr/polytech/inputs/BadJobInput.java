package fr.polytech.inputs;

import javax.xml.ws.WebFault;

@WebFault(name = "BadJobFault", targetNamespace = "http://informatique.polytech.unice.fr/soa1/order")
public class BadJobInput extends Exception {

    public BadJobInput(String value) {
        super("Bad Job: [" + value + "]");
    }
}
