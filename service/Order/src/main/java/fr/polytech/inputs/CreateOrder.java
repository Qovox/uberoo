package fr.polytech.inputs;

import fr.polytech.KafkaConsumerUtils;
import fr.polytech.KafkaProducerUtils;
import fr.polytech.business.Order;
import fr.polytech.db.ConnectionJDBC;
import fr.polytech.outputs.JobResult;
import fr.polytech.outputs.MessageOutput;
import fr.polytech.outputs.OrderOutput;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.json.JSONObject;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import static fr.polytech.business.EventType.*;

@XmlType(name = "create_order")
public class CreateOrder extends JobInput {

    @XmlElement(name = "order", required = true)
    private Order order;


    @Override
    protected JobResult run() throws DataBaseException {
        String id = ((Double) Math.random()).toString();

        try {
            boolean hasAlreadyOrder = ConnectionJDBC.hasAlreadyOrder(order);
            if(hasAlreadyOrder){
                MessageOutput result = new MessageOutput();
                result.setMessage("This order already exists");
                return result;
            } else {
                KafkaProducerUtils.produceEvent(CONFIRM_CATALOG.getTopic(), id, order.toJsonObject());

                // orderJsonObject.put("restaurantId", 4); // for testing (simulate catalog)
                // KafkaProducerUtils.produceEvent("Catalog_OK", id, orderJsonObject); // for testing (simulate catalog)
            }
        } catch (SQLException e) {
            throw new DataBaseException("Service not available (database problem)");

        }

        ConsumerRecord<String, JSONObject> record = KafkaConsumerUtils.consumeEventWithSpecificGroupId(Arrays.asList(ORDER_CREATED.getTopic(), ORDER_CANCELED.getTopic(), DATABASE_PROBLEM.getTopic()), id);
        if (record.topic().equals(ORDER_CREATED.getTopic())) {
            OrderOutput result = new OrderOutput();
            result.setOrder(new Order(record.value()));
            return result;
        } else if (record.topic().equals(ORDER_CANCELED.getTopic())) {
            MessageOutput result = new MessageOutput();
            result.setMessage("Order not created: Wrong products");
            return result;
        } else {
            throw new DataBaseException("Service not available (database problem)");
        }

    }

    @Override
    protected void check() throws BadJobInput {
        if (this.order == null || this.order.getCustomerName() == null || this.order.getProductIds() == null || this.order.getDate() == null || this.order.getAdress() == null) {
            throw new BadJobInput("invalid arguments");
        }

        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime.parse(order.getDate(), formatter);
        } catch (Exception e) {
            throw new BadJobInput("invalid date");
        }
    }
}
