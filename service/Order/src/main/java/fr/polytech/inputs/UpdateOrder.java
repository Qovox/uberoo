package fr.polytech.inputs;

import fr.polytech.KafkaConsumerUtils;
import fr.polytech.KafkaProducerUtils;
import fr.polytech.business.EventType;
import fr.polytech.business.OrderState;
import fr.polytech.business.ServiceType;
import fr.polytech.db.ConnectionJDBC;
import fr.polytech.outputs.JobResult;
import fr.polytech.outputs.MessageOutput;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.json.JSONObject;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.EventListener;

@XmlType(name = "update_order")
public class UpdateOrder extends JobInput {

    @XmlElement(name = "orderId", required = true)
    private int orderId;

    @XmlElement(name = "state", required = true)
    private OrderState state;

    @Override
    protected JobResult run() throws DataBaseException {

        JSONObject updateStateJsonObject = new JSONObject();
        updateStateJsonObject.put("orderId", orderId);
        updateStateJsonObject.put("state", state.toString());

        try{
            ConnectionJDBC.updateState(updateStateJsonObject.getInt("orderId"), updateStateJsonObject.getString("state"));
            if(this.state == OrderState.PREPARED){
                KafkaProducerUtils.produceEvent(EventType.ORDER_PREPARED.getTopic(), "", updateStateJsonObject);
            }
            MessageOutput result = new MessageOutput();
            result.setMessage("state changed");
            return result;
        } catch (SQLException e) {
            throw new DataBaseException("Service not available (database problem)");
        }
    }

    @Override
    protected void check() throws BadJobInput {
        if (state == null || orderId == 0) {
            throw new BadJobInput("invalid state");
        }
    }
}
