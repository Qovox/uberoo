package fr.polytech.inputs;

import javax.xml.ws.WebFault;

@WebFault(name = "DataBaseException", targetNamespace = "http://informatique.polytech.unice.fr/soa1/order")
public class DataBaseException extends Exception {

    public DataBaseException(String value) {
        super("DataBaseException: [" + value + "]");
    }
}
