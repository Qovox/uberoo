package fr.polytech.inputs;

import fr.polytech.outputs.JobResult;

import javax.xml.bind.annotation.XmlSeeAlso;

@XmlSeeAlso({CreateOrder.class, ReadOrdersByRestaurant.class, ReadReviewsByRestaurant.class, UpdateOrder.class, ReadOrder.class, ReviewOrder.class})
public abstract class JobInput {

    public final JobResult process() throws BadJobInput, DataBaseException {
        check();
        return run();
    }

    abstract protected JobResult run() throws BadJobInput, DataBaseException;

    abstract protected void check() throws BadJobInput;
}
