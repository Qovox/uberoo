package fr.polytech.inputs;

import fr.polytech.business.Review;
import fr.polytech.db.ConnectionJDBC;
import fr.polytech.outputs.JobResult;
import fr.polytech.outputs.ListReviews;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.sql.SQLException;
import java.util.List;

@XmlType(name = "read_reviews_by_restaurant")
public class ReadReviewsByRestaurant extends JobInput {

    @XmlElement(name = "restaurantId", required = true)
    private int restaurantId;

    @Override
    protected JobResult run() throws DataBaseException {
        ListReviews result = new ListReviews();
        try {
            List<Review> reviews = ConnectionJDBC.getReviewsByRestaurantId(restaurantId);
            result.setReviews(reviews);
            return result;
        } catch (SQLException e) {
            throw new DataBaseException("Service not available (database problem)");
        }
    }

    @Override
    protected void check() throws BadJobInput {
        if (restaurantId == 0) {
            throw new BadJobInput("invalid arguments");
        }
    }
}
