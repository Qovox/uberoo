package fr.polytech.inputs;


import fr.polytech.business.Order;
import fr.polytech.db.ConnectionJDBC;
import fr.polytech.outputs.JobResult;
import fr.polytech.outputs.OrderOutput;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.sql.SQLException;
import java.util.Optional;

@XmlType(name = "read_order")
public class ReadOrder extends JobInput {

    @XmlElement(name = "orderId", required = true)
    private int orderId;

    @Override
    protected JobResult run() throws BadJobInput, DataBaseException {
        OrderOutput result = new OrderOutput();
        try {
            Optional<Order> order = ConnectionJDBC.getOrdersById(orderId);
            if (!order.isPresent()) {
                throw new BadJobInput("There is no Order with id = " + orderId);
            }
            result.setOrder(order.get());
            return result;
        } catch (SQLException e) {
            throw new DataBaseException("Service not available (database problem)");
        }
    }

    @Override
    protected void check() throws BadJobInput {
        if (orderId == 0) {
            throw new BadJobInput("invalid Id");
        }
    }
}
