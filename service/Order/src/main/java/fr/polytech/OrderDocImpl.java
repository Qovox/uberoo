package fr.polytech;

import fr.polytech.inputs.BadJobInput;
import fr.polytech.inputs.DataBaseException;
import fr.polytech.inputs.JobInput;
import fr.polytech.outputs.JobResult;

import javax.jws.WebService;

@WebService(targetNamespace = "http://informatique.polytech.unice.fr/soa1/order/public",
        portName = "OrderPort",
        serviceName = "OrderService",
        endpointInterface = "fr.polytech.OrderDoc")
public class OrderDocImpl implements OrderDoc {

    @Override
    public JobResult dispatch(JobInput input) throws BadJobInput, DataBaseException {
        return input.process();
    }
}
