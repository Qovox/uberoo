DROP DATABASE IF EXISTS catalogdb;
DROP DATABASE IF EXISTS uberoo;
DROP DATABASE IF EXISTS paymentdb;
DROP DATABASE IF EXISTS coursierdb;

CREATE DATABASE catalogdb;
USE catalogdb;

CREATE TABLE IF NOT EXISTS restaurants (
	id INT(10) NOT NULL,
	`name` VARCHAR(45) NOT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS categories (
	id INT(10) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS items (
	id INT(10) NOT NULL,
	`name` VARCHAR(45) NOT NULL,
  price float(10) NOT NULL,
  `type` VARCHAR(20) NOT NULL,
  restaurant_id INT(10) NOT NULL,
	PRIMARY KEY (id),
  FOREIGN KEY (restaurant_id) REFERENCES restaurants(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS categories_items_binding (
	item_id INT(10) NOT NULL,
  category_id INT(10) NOT NULL,
	PRIMARY KEY (item_id, category_id),
  FOREIGN KEY (item_id) REFERENCES items(id),
  FOREIGN KEY (category_id) REFERENCES categories(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS promo_codes (
	id VARCHAR(50) NOT NULL,
  `type` VARCHAR(20) NOT NULL,
  discount float(10) NOT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS composition_items_binding (
	item_type VARCHAR(20) NOT NULL,
  promo_code_id VARCHAR(50) NOT NULL,
	PRIMARY KEY (item_type, promo_code_id),
  FOREIGN KEY (promo_code_id) REFERENCES promo_codes(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT IGNORE INTO restaurants (id, `name`) VALUES (1, "Kow Long"); 
INSERT IGNORE INTO restaurants (id, `name`) VALUES (2, "Song Qi"); 
INSERT IGNORE INTO restaurants (id, `name`) VALUES (3, "Ambata"); 
INSERT IGNORE INTO restaurants (id, `name`) VALUES (4, "Luz Verde"); 
INSERT IGNORE INTO restaurants (id, `name`) VALUES (5, "Domani"); 
INSERT IGNORE INTO restaurants (id, `name`) VALUES (6, "L'espelette"); 
INSERT IGNORE INTO restaurants (id, `name`) VALUES (7, "Pizza d'Or"); 
INSERT IGNORE INTO restaurants (id, `name`) VALUES (8, "Arzak"); 
INSERT IGNORE INTO restaurants (id, `name`) VALUES (9, "Sushi Saito");

INSERT IGNORE INTO categories (id, `name`) VALUES (1, "Chinese");
INSERT IGNORE INTO categories (id, `name`) VALUES (2, "Dessert");
INSERT IGNORE INTO categories (id, `name`) VALUES (3, "Mexican");
INSERT IGNORE INTO categories (id, `name`) VALUES (4, "Pizza");
INSERT IGNORE INTO categories (id, `name`) VALUES (5, "Spanish");
INSERT IGNORE INTO categories (id, `name`) VALUES (6, "Spicy");
INSERT IGNORE INTO categories (id, `name`) VALUES (7, "Starter");
INSERT IGNORE INTO categories (id, `name`) VALUES (8, "Sweet_sour");

INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (1, "Kung Pao Chicken", 10.50, "MAIN_COURSE", 1);
INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (2, "Ma Po Tofu", 9.0, "MAIN_COURSE", 1);
INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (3, "Tiramisu", 5.0, "DESSERT", 3);
INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (4, "Floating island", 5.0, "DESSERT", 3);
INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (5, "Chocolate mousse", 5.0, "DESSERT", 9);
INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (6, "Tacos", 6.75, "MAIN_COURSE", 4);
INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (7, "Quesadillas", 6.25, "MAIN_COURSE", 4);
INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (8, "Buldak", 8.0, "MAIN_COURSE", 5);
INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (9, "Abiko Curry", 12.0, "MAIN_COURSE", 5);
INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (10, "Margherita", 11.0, "MAIN_COURSE", 6);
INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (11, "Napoli", 10.75, "MAIN_COURSE", 6);
INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (12, "Napoli", 10.50, "MAIN_COURSE", 7);
INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (13, "Gazpachuelo", 13.10, "MAIN_COURSE", 3);
INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (14, "Paella", 19.80, "MAIN_COURSE", 8);
INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (15, "Watermelon and feta", 12.20, "ENTRY", 3);
INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (16, "Sushi salad", 14.50, "ENTRY", 9);
INSERT IGNORE INTO items (id, `name`, price, `type`, restaurant_id) VALUES (17, "Sweat and Sour Pork", 15.0, "MAIN_COURSE", 9);

INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (1, 1);
INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (2, 1);
INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (17, 1);

INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (3, 2);
INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (4, 2);
INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (5, 2);

INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (6, 3);
INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (7, 3);

INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (10, 4);
INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (11, 4);
INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (12, 4);

INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (13, 5);
INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (14, 5);

INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (2, 6);
INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (8, 6);
INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (9, 6);

INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (15, 7);
INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (16, 7);

INSERT IGNORE INTO categories_items_binding (item_id, category_id) VALUES (17, 8);

INSERT IGNORE INTO promo_codes (id, `type`, discount) VALUES ("Xmas", "DIRECT", 25.0);
INSERT IGNORE INTO promo_codes (id, `type`, discount) VALUES ("Uberoo", "COMPOSITION", 10.0);

INSERT IGNORE INTO composition_items_binding (item_type, promo_code_id) VALUES ("ENTRY", "Uberoo");
INSERT IGNORE INTO composition_items_binding (item_type, promo_code_id) VALUES ("MAIN_COURSE", "Uberoo");
INSERT IGNORE INTO composition_items_binding (item_type, promo_code_id) VALUES ("DESSERT", "Uberoo");

SELECT * FROM categories_items_binding;
SELECT * FROM items;
SELECT * FROM categories;
SELECT * FROM restaurants;
SELECT * FROM promo_codes;
SELECT * FROM composition_items_binding;

CREATE DATABASE uberoo;
USE uberoo;

CREATE TABLE orders (
  id INT(11) NOT NULL AUTO_INCREMENT,
  date_creation VARCHAR(50) NOT NULL,
  restaurantId INT(11) NOT NULL,
  state VARCHAR(20) NOT NULL,
  customerName VARCHAR(30) NOT NULL,
  adress VARCHAR(70) NOT NULL,
  primary KEY (id)
) ENGINE=INNODB;

CREATE TABLE products_ordered(
  id INT(11) NOT NULL,
  product_id INT(11) NOT NULL,
  primary KEY (id, product_id),
  FOREIGN KEY(id) REFERENCES orders(id)
) ENGINE=INNODB;

insert INTO orders (date_creation, restaurantId, state, customerName, adress) VALUES ('2018-10-07 10:45:32', 2, 'ORDERED', 'Seb', '5 rue Michel 06560 Valbonne');
insert INTO orders (date_creation, restaurantId, state, customerName, adress) VALUES ('2018-10-07 14:12:27', 4, 'COOKING', 'Alice', 'Lille');
insert INTO orders (date_creation, restaurantId, state, customerName, adress) VALUES ('2018-10-07 17:32:57', 1, 'ORDERED', 'John', 'Place Republique');

insert INTO products_ordered (id, product_id) VALUES (1, 5);
insert INTO products_ordered (id, product_id) VALUES (2, 4);
insert INTO products_ordered (id, product_id) VALUES (2, 6);
insert INTO products_ordered (id, product_id) VALUES (3, 3);

SELECT * FROM orders;
SELECT * FROM products_ordered;

CREATE DATABASE paymentdb;
USE paymentdb;

CREATE TABLE payments (
  id INT(10) NOT NULL,
  customer_name VARCHAR(30) NOT NULL,
  mode VARCHAR(10) NOT NULL,
  status VARCHAR(10) NOT NULL,
  credit_card_number INT8(12),
  amount FLOAT(10),
  PRIMARY KEY (id)
) ENGINE=InnoDB;

CREATE TABLE payment_items_binding (
  id INT(10) NOT NULL,
  item_id INT(10) NOT NULL,
  PRIMARY KEY (id, item_id),
  FOREIGN KEY (id) REFERENCES payments(id)
) ENGINE=InnoDB;

/* Previous mocked payments */
INSERT IGNORE INTO payments (id, customer_name, mode, status) VALUES (1, 'Seb', 'UNKNOWN', 'NOT_PAID');
INSERT IGNORE INTO payments (id, customer_name, mode, status, credit_card_number, amount) VALUES (2, 'Alice', 'ONLINE', 'PAID', 123456789012, 11.75);
INSERT IGNORE INTO payments (id, customer_name, mode, status) VALUES (3, 'John', 'UNKNOWN', 'NOT_PAID');

INSERT IGNORE INTO payment_items_binding (id, item_id) VALUES (1, 5);
INSERT IGNORE INTO payment_items_binding (id, item_id) VALUES (2, 4);
INSERT IGNORE INTO payment_items_binding (id, item_id) VALUES (2, 6);
INSERT IGNORE INTO payment_items_binding (id, item_id) VALUES (3, 3);

SELECT * FROM payments;
SELECT * FROM payment_items_binding;

CREATE DATABASE coursierdb;
USE coursierdb;

CREATE TABLE IF NOT EXISTS coursier (
  id INT(10) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS orders (
  id INT(10) NOT NULL,
  `customer_address` VARCHAR(45) NOT NULL,
  `current_address` VARCHAR(45) NOT NULL,
  current_state INT(10) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS restaurant (
  id INT(10) NOT NULL,
  `restaurant_address` VARCHAR(45) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS binding_order_coursier (
  coursier_id INT(10) NOT NULL,
  order_id INT(10) NOT NULL,
  PRIMARY KEY (coursier_id, order_id),
  FOREIGN KEY (coursier_id) REFERENCES coursier(id),
  FOREIGN KEY (order_id) REFERENCES orders(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS binding_coursier_position (
  coursier_id INT(10) NOT NULL,
  address VARCHAR(45) NOT NULL,
  PRIMARY KEY (coursier_id),
  FOREIGN KEY (coursier_id) REFERENCES coursier(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT IGNORE INTO restaurant (id, `restaurant_address`) VALUES (1, "1 rue du restaurant");
INSERT IGNORE INTO restaurant (id, `restaurant_address`) VALUES (2, "4 rue du restaurant");
INSERT IGNORE INTO restaurant (id, `restaurant_address`) VALUES (3, "6 rue du restaurant");
INSERT IGNORE INTO restaurant (id, `restaurant_address`) VALUES (4, "42 rue du restaurant");
INSERT IGNORE INTO restaurant (id, `restaurant_address`) VALUES (5, "65 rue du restaurant");

INSERT IGNORE INTO orders (id, `customer_address`, `current_address`, current_state) VALUES (1, "5 route des vignes", "1 rue du restaurant", 2);
INSERT IGNORE INTO orders (id, `customer_address`, `current_address`, current_state) VALUES (2, "54 route des acacias", "4 rue du restaurant", 1);
INSERT IGNORE INTO orders (id, `customer_address`, `current_address`, current_state) VALUES (3, "2 chemin de l'hopital", "6 rue du restaurant", 2);
INSERT IGNORE INTO orders (id, `customer_address`, `current_address`, current_state) VALUES (4, "87 avenue montaigne", "42 rue du restaurant", 1);
INSERT IGNORE INTO orders (id, `customer_address`, `current_address`, current_state) VALUES (5, "4 rue de la statue", "65 rue du restaurant", 2);
INSERT IGNORE INTO orders (id, `customer_address`, `current_address`, current_state) VALUES (6, "10 route des vignes", "1 rue du restaurant", 1);
INSERT IGNORE INTO orders (id, `customer_address`, `current_address`, current_state) VALUES (7, "22 chemin de droit", "4 rue du restaurant", 2);
INSERT IGNORE INTO orders (id, `customer_address`, `current_address`, current_state) VALUES (8, "974 route napoleon", "6 rue du restaurant", 2);

INSERT IGNORE INTO binding_order_coursier (coursier_id, order_id) VALUES (1, 1);
INSERT IGNORE INTO binding_order_coursier (coursier_id, order_id) VALUES (2, 3);
INSERT IGNORE INTO binding_order_coursier (coursier_id, order_id) VALUES (3, 5);
INSERT IGNORE INTO binding_order_coursier (coursier_id, order_id) VALUES (4, 7);
INSERT IGNORE INTO binding_order_coursier (coursier_id, order_id) VALUES (5, 8);

INSERT IGNORE INTO binding_coursier_position (coursier_id, address) VALUES (1, "1 rue du restaurant");
INSERT IGNORE INTO binding_coursier_position (coursier_id, address) VALUES (2, "4 rue du restaurant");
INSERT IGNORE INTO binding_coursier_position (coursier_id, address) VALUES (3, "6 rue du restaurant");
INSERT IGNORE INTO binding_coursier_position (coursier_id, address) VALUES (4, "42 rue du restaurant");

SELECT * FROM binding_order_coursier;
SELECT * FROM binding_coursier_position;
SELECT * FROM restaurant;
SELECT * FROM coursier;
SELECT * FROM orders;