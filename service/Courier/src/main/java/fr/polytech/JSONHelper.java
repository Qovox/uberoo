package fr.polytech;

import org.json.JSONObject;

import javax.ejb.Singleton;
import javax.ejb.Startup;

@Startup
@Singleton(name = "JSONHelper")
public class JSONHelper {

    public JSONObject preparePickedMessage(int courierId, int orderId){
        JSONObject res = new JSONObject();

        res.put("courierId", courierId);
        res.put("orderId", orderId);

        return res;
    }

    public JSONObject prepareDeliveredMessage(int courierId, int orderId){
        JSONObject res = new JSONObject();

        res.put("courierId", courierId);
        res.put("orderId", orderId);

        return res;
    }

}
