package fr.polytech;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public interface CourierREST {

    @POST
    @Path("/notifyOrderDelivered")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    Response notifyOrderDelivered(@FormParam("courierId") int courierId, @FormParam("orderId") int orderId);

    @POST
    @Path("/pickOrderToDeliver")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    Response pickOrderToDeliver(@FormParam("courierId") int courierId, @FormParam("orderId") int orderId);

    @POST
    @Path("/notifyProblem")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    Response notifyProblem(@FormParam("orderId") int orderId, @FormParam("courierId") int courierId);

    @GET
    @Path("/{courierId}/orderList")
    Response readOrderToDeliver(@PathParam("courierId") int courierId);

    @GET
    @Path("/nearOrders")
    Response getNearOrders(@QueryParam("address") String actualAddress);
}
