package fr.polytech;

import fr.polytech.business.EventType;
import fr.polytech.db.ConnectionJDBC;
import fr.polytech.exception.NoSuchCourierException;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.logging.Logger;

@Path("/")
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class CourierRESTImpl implements CourierREST {

    private static final Logger log = Logger.getLogger(Logger.class.getName());

    private ConnectionJDBC connectionJDBC = new ConnectionJDBC();

    @EJB
    private JSONHelper helper;

    @GET
    @Produces("text/plain")
    public String help() {
        return "Home page is http://localhost:8080 or http://localhost:8080/courier\n" +
                "Possibilities : /notify, /{X}/orderList\n";
    }

    @POST
    @Path("/pickOrderToDeliver")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response pickOrderToDeliver(@FormParam("courierId") int courierId, @FormParam("orderId") int orderId) {
        log.info("Courier with id " + courierId + " wants to deliver order " + orderId);

        if (this.connectionJDBC.associatedCourierWithOrder(courierId, orderId)){
            KafkaProducerUtils.produceEvent(EventType.ORDER_PICKED.getTopic(), "", this.helper.preparePickedMessage(courierId, orderId));

            return Response.ok("Courier with id " + courierId + " will deliver order " + orderId).build();
        }

        return Response.status(Response.Status.NOT_MODIFIED).entity("Server can't associate the courier " + courierId + "qith the order " + orderId).build();
    }

    @POST
    @Path("/notifyOrderDelivered")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response notifyOrderDelivered(@FormParam("courierId") int courierId, @FormParam("orderId") int orderId) {
        log.info("The order with id " + orderId + " is now delivered");

        if (this.connectionJDBC.wasDelivered(orderId)){
            KafkaProducerUtils.produceEvent(EventType.ORDER_DELIVERED.getTopic(), "", this.helper.prepareDeliveredMessage(courierId, orderId));

            return Response.ok("Courier with id " + courierId + " delivered order " + orderId).build();
        }

        return Response.status(Response.Status.NOT_MODIFIED).entity("Server can't end the courier " + courierId + " delivery for the order " + orderId).build();
    }

    @POST
    @Path("/notifyProblem")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response notifyProblem(@FormParam("orderId") int orderId, @FormParam("courierId") int courierId) {
        log.info("A courier with ID " + courierId + " notified that he can't finish the deliver with ID " + orderId);

        //TODO Add call to geotracking MS

        this.connectionJDBC.courierTrouble(courierId, orderId, "");

        return Response.ok("courier will not deliver order with ID " + orderId).build();
    }

    @GET
    @Path("/{courierId}/orderList")
    @Override
    public Response readOrderToDeliver(@PathParam("courierId") int courierId) {
        log.info("A courier with ID " + courierId + " requested to see the orders associated with him");

        try {
            return Response.ok(connectionJDBC.getOrderByCourierId(courierId)).build();
        } catch (NoSuchCourierException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("/nearOrders")
    @Override
    public Response getNearOrders(@QueryParam("address") String actualAddress) {
        if (actualAddress == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Address can't be null").build();
        }

        log.info("A courier requested the orders near the following address : " + actualAddress);

        return Response.ok(connectionJDBC.getOrderByAddress(actualAddress)).build();
    }
}
