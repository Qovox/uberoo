package fr.polytech.job;

import fr.polytech.KafkaConsumerUtils;
import fr.polytech.business.DeliverableOrder;
import fr.polytech.business.ServiceType;
import fr.polytech.db.ConnectionJDBC;
import fr.polytech.listeners.KafkaEventListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.json.JSONObject;

import javax.ejb.EJB;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.polytech.business.EventType;

public class JobListener implements KafkaEventListener {

    private ConnectionJDBC dao = new ConnectionJDBC();

    private List<Thread> threads = new ArrayList<>();

    public void startListeners() {
        Thread orderCreatedListener = new Thread(this::startOrderCreatedListener);
        Thread orderStateUpdated = new Thread(this::startOrderPreparedListener);
        Thread courierPositionUpdated = new Thread(this::startCourierPositionListener);

        orderCreatedListener.start();
        orderStateUpdated.start();
        courierPositionUpdated.start();

        threads.add(orderCreatedListener);
        threads.add(orderStateUpdated);
        threads.add(courierPositionUpdated);
    }

    public void stopListeners() {
        for (Thread thread : threads) {
            thread.interrupt();
        }
    }

    private void startCourierPositionListener() {
        while (true) {
            ConsumerRecords<String, JSONObject> consumerRecords = KafkaConsumerUtils.consumeEvents(Collections.singletonList(EventType.COURIER_POSITION.getTopic()), ServiceType.COURIER);
            for (ConsumerRecord<String, JSONObject> consumerRecord : consumerRecords) {
                updateOrderPosition(consumerRecord.key(), consumerRecord.value());
            }
        }
    }

    private void startOrderPreparedListener() {
        while (true) {
            ConsumerRecords<String, JSONObject> consumerRecords = KafkaConsumerUtils.consumeEvents(Collections.singletonList(EventType.ORDER_PREPARED.getTopic()), ServiceType.COURIER);
            for (ConsumerRecord<String, JSONObject> consumerRecord : consumerRecords) {
                updateNewOrder(consumerRecord.key(), consumerRecord.value());
            }
        }
    }

    private void startOrderCreatedListener() {
        while (true) {
            ConsumerRecords<String, JSONObject> consumerRecords = KafkaConsumerUtils.consumeEvents(Collections.singletonList(EventType.ORDER_CREATED.getTopic()), ServiceType.COURIER);
            for (ConsumerRecord<String, JSONObject> consumerRecord : consumerRecords) {
                initNewOrder(consumerRecord.key(), consumerRecord.value());
            }
        }
    }

    private void initNewOrder(String id, JSONObject value) {

        DeliverableOrder toProcess = new DeliverableOrder(
                value.getInt("orderId"),
                "",
                value.getString("address")
        );

        this.dao.insertNewOrder(toProcess, value.getInt("restaurantId"));
    }


    private void updateNewOrder(String id, JSONObject value) {
        this.dao.activateOrder(value.getInt("orderId"));
    }

    private void updateOrderPosition(String key, JSONObject value) {
        this.dao.updateOrderPosition(value.getString("courierId"), value.getString("address"));
    }
}
