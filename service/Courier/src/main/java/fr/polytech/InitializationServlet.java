package fr.polytech;

import fr.polytech.job.JobListener;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class InitializationServlet implements ServletContextListener {

    private JobListener eventListener = new JobListener();

    @Override
    public void contextInitialized(ServletContextEvent contextEvent) {
        eventListener.startListeners();
    }

    @Override
    public void contextDestroyed(ServletContextEvent contextEvent) {
        eventListener.stopListeners();
    }
}
