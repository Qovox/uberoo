package fr.polytech.business;

import javax.xml.bind.annotation.*;

@XmlType
@XmlRootElement(name = "deliverable")
public class DeliverableOrder {

    private int orderId;
    private String restaurantAddress;
    private String customerAddress;

    public DeliverableOrder() {

    }

    public DeliverableOrder(int orderId, String restaurantAddress, String customerAddress) {
        this.orderId = orderId;
        this.restaurantAddress = restaurantAddress;
        this.customerAddress = customerAddress;
    }

    @XmlAttribute(name = "id")
    @XmlID
    public int getOrderId() {
        return orderId;
    }

    @XmlElement(name = "restaurantAddress")
    public String getRestaurantAddress() {
        return restaurantAddress;
    }

    @XmlElement(name = "customerAddress")
    public String getCustomerAddress() {
        return customerAddress;
    }
}
