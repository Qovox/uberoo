package fr.polytech.business;

import java.util.ArrayList;
import java.util.List;

public class Courier {

    private int id;
    private String name;
    private List<DeliverableOrder> orderList;

    public Courier(String name, int id) {
        this.id = id;
        this.name = name;
        this.orderList = new ArrayList<>();
    }

    public int getId() {
        return id;
    }
}
