package fr.polytech.db;

import fr.polytech.business.DeliverableOrder;
import fr.polytech.exception.NoSuchCourierException;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectionJDBC {

    private static final Logger log = Logger.getLogger(Logger.class.getName());

    // Db config
    private static ResourceBundle bundle = ResourceBundle.getBundle("configDB");
    private static String urlParameters = "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false";
    private static String url = bundle.getString("jdbc.url") + bundle.getString("jdbc.databaseName") + urlParameters;
    private static String user = bundle.getString("jdbc.user");
    private static String password = bundle.getString("jdbc.password");

    // Db connection handling
    private Connection connexion = null;
    private Statement statement = null;

    public ConnectionJDBC() {
        init();
    }

    private void init() {

    }

    private void connect() throws SQLException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException("Cannot find the driver in the classpath!", e);
        }

        connexion = DriverManager.getConnection(url, user, password);
        statement = connexion.createStatement();
    }

    private void close() {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException ignore) {
                log.log(Level.SEVERE, "MySQL Statement failed closing");
            }
        }
        if (connexion != null) {
            try {
                connexion.close();
            } catch (SQLException ignore) {
                log.log(Level.SEVERE, "MySQL Connection failed closing");
            }
        }
    }

    private void close(ResultSet result) {
        if (result != null) {
            try {
                result.close();
            } catch (SQLException ignore) {
                log.log(Level.SEVERE, "MySQL ResultSet failed closing");
            }
        }

        this.close();
    }

    public void insertNewOrder(DeliverableOrder order, int restaurantId) {
        try {
            connect();

            String query = "INSERT IGNORE INTO orders (id, `customer_address`, `current_address`, current_state)"
                    + " VALUES (?, ?, ?, ?)";

            PreparedStatement preparedStmt = connexion.prepareStatement(query);
            preparedStmt.setInt(1, order.getOrderId());
            preparedStmt.setString(2, order.getCustomerAddress());
            preparedStmt.setString(3, retrieveAddressFromID(restaurantId));
            preparedStmt.setInt(4, 0);

            preparedStmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }

    private String retrieveAddressFromID(int restaurantId) {
        ResultSet result = null;

        try {
            connect();

            result = statement.executeQuery("SELECT * FROM restaurant WHERE id = " + restaurantId + ";");

            if (result.next()) {
                return result.getString("restaurant_address");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(result);
        }

        return "";
    }

    public List<DeliverableOrder> getOrderByCourierId(int courierId) throws NoSuchCourierException {
        List<DeliverableOrder> orders = new ArrayList<>();
        ResultSet result = null;

        try {

            for (Integer integer :
                    this.getOrderIdFromBinding(courierId)) {

                connect();

                result = statement.executeQuery("SELECT * FROM orders WHERE id = " + integer + ";");

                while (result.next()) {
                    orders.add(new DeliverableOrder(result.getInt("id"), result.getString("current_address"), result.getString("customer_address")));
                }
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, "MySQL Request failed - " + e.getMessage());
        } finally {
            close(result);
        }

        return orders;
    }

    private List<Integer> getOrderIdFromBinding(int courierId) throws NoSuchCourierException {
        List<Integer> orders = new ArrayList<>();
        ResultSet result = null;

        try {
            connect();

            result = statement.executeQuery("SELECT * FROM binding_order_coursier WHERE coursier_id = " + courierId + ";");

            while (result.next()) {
                orders.add(result.getInt("order_id"));
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, "MySQL Request failed - " + e.getMessage());
        } finally {
            close(result);
        }

        return orders;
    }

    public List<DeliverableOrder> getOrderByAddress(String actualAddress) {
        List<DeliverableOrder> orders = new ArrayList<>();
        ResultSet result = null;

        try {
            connect();

            result = statement.executeQuery("SELECT * FROM orders WHERE customer_address = \"" + actualAddress + "\" AND current_state = 1;");

            while (result.next()) {
                orders.add(new DeliverableOrder(result.getInt("id"), result.getString("current_address"), result.getString("customer_address")));
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, "MySQL Request failed - " + e.getMessage());
        } finally {
            close(result);
        }

        return orders;
    }

    public boolean wasDelivered(int orderId) {
        try {
            connect();

            statement.executeUpdate("DELETE FROM binding_order_coursier WHERE order_id = " + orderId + ";");

            statement.executeUpdate("UPDATE orders SET current_state = 3 WHERE id = " + orderId + ";");

            return true;
        } catch (SQLException e) {
            log.log(Level.SEVERE, "MySQL Request failed - " + e.getMessage());
            return false;
        } finally {
            close();
        }
    }

    public boolean associatedCourierWithOrder(int courierId, int orderId) {
        try {
            connect();

            String query = "INSERT IGNORE INTO binding_order_coursier (coursier_id, order_id)"
                    + " VALUES (?, ?)";

            PreparedStatement preparedStmt = connexion.prepareStatement(query);
            preparedStmt.setInt(1, courierId);
            preparedStmt.setInt(2, orderId);

            preparedStmt.execute();

            statement.executeUpdate("UPDATE orders SET current_state = 2 WHERE id = " + orderId + ";");

            return true;
        } catch (SQLException e) {
            log.log(Level.SEVERE, "MySQL Request failed - " + e.getMessage());
            return false;
        } finally {
            close();
        }
    }

    public void courierTrouble(int courierId, int orderId, String position) {
        try {
            connect();

            statement.executeUpdate("DELETE FROM binding_order_coursier WHERE order_id = " + orderId + " AND coursier_id = " + courierId + ";");

            statement.executeUpdate("UPDATE orders SET current_state = 1, current_address = \"" + position + "\" WHERE id = " + orderId + ";");

        } catch (SQLException e) {
            log.log(Level.SEVERE, "MySQL Request failed - " + e.getMessage());
        } finally {
            close();
        }
    }

    public void activateOrder(int orderId) {
        try {
            connect();

            statement.executeUpdate("UPDATE orders SET current_state = 1 WHERE id = " + orderId + ";");
        } catch (SQLException e) {
            log.log(Level.SEVERE, "MySQL Request failed - " + e.getMessage());
        } finally {
            close();
        }
    }

    public void updateOrderPosition(String courierId, String address) {
        ResultSet result = null;

        try {
            connect();

            result = statement.executeQuery("SELECT * FROM binding_order_coursier WHERE coursier_id = " + courierId + ";");

            if (result.next()) {
                statement.executeUpdate("UPDATE orders SET current_address = \"" + address + "\" WHERE id = " + result.getInt("order_id") + ";");
            }

        } catch (SQLException e) {
            log.log(Level.SEVERE, "MySQL Request failed - " + e.getMessage());
        } finally {
            close(result);
        }
    }
}
