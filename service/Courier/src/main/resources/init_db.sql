/*
USE coursierdb;

DROP TABLE IF EXISTS binding_order_coursier;
DROP TABLE IF EXISTS binding_coursier_position;
DROP TABLE IF EXISTS restaurant;
DROP TABLE IF EXISTS coursier;
DROP TABLE IF EXISTS orders;
*/

CREATE TABLE IF NOT EXISTS coursier (
  id int(10) NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS orders (
  id int(10) NOT NULL,
  `customer_address` varchar(45) NOT NULL,
  `current_address` varchar(45) NOT NULL,
  current_state int(10) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS restaurant (
  id INT(10) NOT NULL,
  `restaurant_address` VARCHAR(45) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS binding_order_coursier (
  coursier_id int(10) NOT NULL,
  order_id int(10) NOT NULL,
  PRIMARY KEY (coursier_id, order_id),
  FOREIGN KEY (coursier_id) REFERENCES coursier(id),
  FOREIGN KEY (order_id) REFERENCES orders(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS binding_coursier_position (
  coursier_id int(10) NOT NULL,
  address varchar(45) NOT NULL,
  PRIMARY KEY (coursier_id),
  FOREIGN KEY (coursier_id) REFERENCES coursier(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT IGNORE INTO coursier (id, `name`) VALUES (1, "michel 1");
INSERT IGNORE INTO coursier (id, `name`) VALUES (2, "michel 2");
INSERT IGNORE INTO coursier (id, `name`) VALUES (3, "michel 3");
INSERT IGNORE INTO coursier (id, `name`) VALUES (4, "michel 4");
INSERT IGNORE INTO coursier (id, `name`) VALUES (5, "michel 5");

INSERT IGNORE INTO restaurant (id, `restaurant_address`) VALUES (1, "1 rue du restaurant");
INSERT IGNORE INTO restaurant (id, `restaurant_address`) VALUES (2, "4 rue du restaurant");
INSERT IGNORE INTO restaurant (id, `restaurant_address`) VALUES (3, "6 rue du restaurant");
INSERT IGNORE INTO restaurant (id, `restaurant_address`) VALUES (4, "42 rue du restaurant");
INSERT IGNORE INTO restaurant (id, `restaurant_address`) VALUES (5, "65 rue du restaurant");

INSERT IGNORE INTO orders (id, `customer_address`, `current_address`, current_state) VALUES (1, "5 route des vignes", "1 rue du restaurant", 2);
INSERT IGNORE INTO orders (id, `customer_address`, `current_address`, current_state) VALUES (2, "54 route des acacias", "4 rue du restaurant", 1);
INSERT IGNORE INTO orders (id, `customer_address`, `current_address`, current_state) VALUES (3, "2 chemin de l'hopital", "6 rue du restaurant", 2);
-- INSERT IGNORE INTO orders (id, `customer_address`, `current_address`, current_state) VALUES (4, "87 avenue montaigne", "42 rue du restaurant", 1);
-- INSERT IGNORE INTO orders (id, `customer_address`, `current_address`, current_state) VALUES (5, "4 rue de la statue", "65 rue du restaurant", 2);
-- INSERT IGNORE INTO orders (id, `customer_address`, `current_address`, current_state) VALUES (6, "10 route des vignes", "1 rue du restaurant", 1);
-- INSERT IGNORE INTO orders (id, `customer_address`, `current_address`, current_state) VALUES (7, "22 chemin de droit", "4 rue du restaurant", 2);
-- INSERT IGNORE INTO orders (id, `customer_address`, `current_address`, current_state) VALUES (8, "974 route napoleon", "6 rue du restaurant", 2);

INSERT IGNORE INTO binding_order_coursier (coursier_id, order_id) VALUES (1, 1);
INSERT IGNORE INTO binding_order_coursier (coursier_id, order_id) VALUES (2, 3);
-- INSERT IGNORE INTO binding_order_coursier (coursier_id, order_id) VALUES (3, 5);
-- INSERT IGNORE INTO binding_order_coursier (coursier_id, order_id) VALUES (4, 7);
-- INSERT IGNORE INTO binding_order_coursier (coursier_id, order_id) VALUES (5, 8);

INSERT IGNORE INTO binding_coursier_position (coursier_id, address) VALUES (1, "1 rue du restaurant");
INSERT IGNORE INTO binding_coursier_position (coursier_id, address) VALUES (2, "4 rue du restaurant");
INSERT IGNORE INTO binding_coursier_position (coursier_id, address) VALUES (3, "6 rue du restaurant");
INSERT IGNORE INTO binding_coursier_position (coursier_id, address) VALUES (4, "42 rue du restaurant");

