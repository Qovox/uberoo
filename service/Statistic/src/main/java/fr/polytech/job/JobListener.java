package fr.polytech.job;

import fr.polytech.KafkaConsumerUtils;
import fr.polytech.business.EventType;
import fr.polytech.business.Order;
import fr.polytech.business.ServiceType;
import fr.polytech.db.ConnectionJDBC;
import fr.polytech.listeners.KafkaEventListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JobListener implements KafkaEventListener {
    private ConnectionJDBC connectionJDBC;

    private List<Thread> threads = new ArrayList<>();

    public void startListeners() {
        Thread createOrderListener = new Thread(() -> {
            startCreateOrderListener();
        });
        Thread PickOrderListener = new Thread(() -> {
            startPickOrderListener();
        });
        Thread orderPreparedListener = new Thread(() -> {
            startOrderPreparedListener();
        });
        Thread orderDeliveredListener = new Thread(() -> {
            startOrderDeliveredListener();
        });
        createOrderListener.start();
        PickOrderListener.start();
        orderPreparedListener.start();
        orderDeliveredListener.start();

        threads.add(createOrderListener);
        threads.add(PickOrderListener);
        threads.add(orderPreparedListener);
        threads.add(orderDeliveredListener);

        connectionJDBC = new ConnectionJDBC();
    }

    public void stopListeners() {
        for (Thread thread : threads) {
            thread.interrupt();
        }
    }

    public void startCreateOrderListener() {
        while (true) {
            ConsumerRecords<String, JSONObject> consumerRecords = KafkaConsumerUtils.consumeEvents(Arrays.asList(EventType.ORDER_CREATED.getTopic()), ServiceType.STATISTIC);
            for (ConsumerRecord<String, JSONObject> consumerRecord : consumerRecords) {
                createOrder(consumerRecord.value());
            }
        }
    }

    public void startPickOrderListener() {
        while (true) {
            ConsumerRecords<String, JSONObject> consumerRecords = KafkaConsumerUtils.consumeEvents(Arrays.asList(EventType.ORDER_PICKED.getTopic()), ServiceType.STATISTIC);
            for (ConsumerRecord<String, JSONObject> consumerRecord : consumerRecords) {
                courierPickOrder(consumerRecord.value());
            }
        }
    }

    public void startOrderPreparedListener() {
        while (true) {
            ConsumerRecords<String, JSONObject> consumerRecords = KafkaConsumerUtils.consumeEvents(Arrays.asList(EventType.ORDER_PREPARED.getTopic()), ServiceType.STATISTIC);
            for (ConsumerRecord<String, JSONObject> consumerRecord : consumerRecords) {
                orderIsReady(consumerRecord.value());
            }
        }
    }

    public void startOrderDeliveredListener() {
        while (true) {
            ConsumerRecords<String, JSONObject> consumerRecords = KafkaConsumerUtils.consumeEvents(Arrays.asList(EventType.ORDER_DELIVERED.getTopic()), ServiceType.STATISTIC);
            for (ConsumerRecord<String, JSONObject> consumerRecord : consumerRecords) {
                orderDelivered(consumerRecord.value());
            }
        }
    }

    public void createOrder(JSONObject orderJsonObject) {
        Order order = new Order(orderJsonObject);
        if (!connectionJDBC.hasAlreadyOrder(order)) {
            connectionJDBC.addOrder(order);
        }
    }

    public void orderIsReady(JSONObject orderJsonObject) {
        connectionJDBC.updateReadyTime(orderJsonObject.getInt("orderId"));
    }

    public void courierPickOrder(JSONObject orderJsonObject) {
        int orderId = orderJsonObject.getInt("orderId");
        int courierId = orderJsonObject.getInt("courierId");
        connectionJDBC.updateCourierId(orderId, courierId);
    }

    public void orderDelivered(JSONObject orderJsonObject) {
        connectionJDBC.updateDeliveredTime(orderJsonObject.getInt("orderId"));
    }
}
