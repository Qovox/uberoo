package fr.polytech;

import fr.polytech.business.Courier;
import fr.polytech.business.ListCourier;
import fr.polytech.business.Order;
import fr.polytech.db.ConnectionJDBC;

import javax.ejb.EJB;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.Duration;
import java.util.*;

@Path("/")
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class StatisticRESTImpl implements StatisticREST {

    @EJB
    private ConnectionJDBC connectionJDBC;

    @Override
    public Response getAverageDeliveryTime(int restaurantId) {
        List<Order> orders = connectionJDBC.getOrdersByRestaurantId(restaurantId);
        long total = 0;
        if (orders.isEmpty()) {
            return Response.ok("no information").build();
        }
        for (Order o : orders) {
            if (o.getTimeDelivered() != o.getTimeReadyToDelivery()) {
                Duration duration = Duration.between(o.getTimeReadyToDelivery(), o.getTimeDelivered());
                total += duration.toMinutes();
            }
        }

        return Response.ok(total / orders.size()).build();
    }

    @Override
    public Response getDeliveryPerCourier(int restaurantId) {
        List<Order> orders = connectionJDBC.getOrdersByRestaurantId(restaurantId);
        if (orders.isEmpty()) {
            return Response.ok("no information").build();
        }
        Map<Integer, Courier> couriers = new HashMap<>();
        for (Order o : orders) {
            if (!couriers.containsKey(o.getIdCourier())) {
                Courier courier = new Courier(o.getIdCourier());
                courier.addOrderDelivered();
                Duration duration = Duration.between(o.getTimeReadyToDelivery(), o.getTimeDelivered());
                courier.setAvgDeliveredTime((courier.getAvgDeliveredTime() * (courier.getOrderDelivered() - 1) + duration.toMinutes()) / courier.getOrderDelivered());
                couriers.put(o.getIdCourier(), courier);
            } else {
                Courier courier = couriers.get(o.getIdCourier());
                courier.addOrderDelivered();
                Duration duration = Duration.between(o.getTimeReadyToDelivery(), o.getTimeDelivered());
                courier.setAvgDeliveredTime((courier.getAvgDeliveredTime() * (courier.getOrderDelivered() - 1) + duration.toMinutes()) / courier.getOrderDelivered());
            }
        }
        ListCourier result = new ListCourier();
        result.setCouriers(new ArrayList<>(couriers.values()));
        return Response.ok(result).build();
    }
}
