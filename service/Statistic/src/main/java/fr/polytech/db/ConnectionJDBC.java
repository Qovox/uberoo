package fr.polytech.db;

import fr.polytech.business.Order;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

@Startup
@Singleton(name = "StatisticPersistence")
public class ConnectionJDBC {

    private static final Logger log = Logger.getLogger(Logger.class.getName());
    private static ResourceBundle bundle = ResourceBundle.getBundle("configDB");
    // mysql db user
    private static final String user = bundle.getString("jdbc.user");
    // mysql db password
    private static final String password = bundle.getString("jdbc.password");
    // mysql db url (need to specify TimeZone as parameter
    private static String urlParameters = "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false";
    private static final String url = bundle.getString("jdbc.url") + bundle.getString("jdbc.databaseName") + urlParameters;
    private Connection connexion = null;
    private Statement statement = null;

    public ConnectionJDBC() {
        init();
    }

    private void init() {

    }

    public boolean hasAlreadyOrder(Order order) {
        ResultSet result = null;
        boolean exist = false;
        try {
            connect();
            // query
            result = statement.executeQuery("SELECT * FROM orders WHERE orderId = '" + order.getIdOrder() + "';");

            // start iterator over the results
            if (result.next()) {

                exist = true;
            }

        } catch (SQLException e) {
            log.log(Level.SEVERE, "MySQL Request failed - " + e.getMessage());
        } finally {
            close();
        }
        return exist;
    }

    public Optional<Order> getOrdersByOrderId(int orderId) {
        ResultSet result = null;
        Optional<Order> orderOptional = Optional.empty();
        try {
            connect();
            // query
            result = statement.executeQuery("SELECT * FROM orders WHERE orderId = " + orderId + ";");

            // start iterator over the results
            if (result.next()) {

                int idOrder = result.getInt("idOrder");
                int idCourier = result.getInt("idCourier");
                int restaurantId = result.getInt("restaurantId");
                LocalDateTime timeReadyToDelivery = LocalDateTime.parse(result.getString("timeReadyToDelivery"));
                LocalDateTime timeDelivered = LocalDateTime.parse(result.getString("timeDelivered"));

                Order order = new Order(restaurantId, idOrder);
                order.setIdCourier(idCourier);
                order.setTimeReadyToDelivery(timeReadyToDelivery);
                order.setTimeDelivered(timeDelivered);

                orderOptional = Optional.of(order);
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, "MySQL Request failed - " + e.getMessage());
        } finally {
            close();
        }
        return orderOptional;
    }

    public List<Order> getOrdersByRestaurantId(int id) {
        ResultSet result = null;
        List<Order> orders = new ArrayList<>();

        try {
            connect();

            result = statement.executeQuery("SELECT * FROM orders WHERE restaurantId = " + id + ";");

            while (result.next()) {
                int idOrder = result.getInt("orderId");
                int idCourier = result.getInt("courierId");
                int restaurantId = result.getInt("restaurantId");
                LocalDateTime timeReadyToDelivery = LocalDateTime.parse(result.getString("timeReadyToDelivery"));
                LocalDateTime timeDelivered = LocalDateTime.parse(result.getString("timeDelivered"));

                Order order = new Order(restaurantId, idOrder);
                order.setIdCourier(idCourier);
                order.setTimeReadyToDelivery(timeReadyToDelivery);
                order.setTimeDelivered(timeDelivered);
                orders.add(order);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            orders = new ArrayList<>();
        } finally {
            close(result);
        }
        return orders;
    }

    public int addOrder(Order order) {
        ResultSet result = null;
        int restaurantId = order.getRestaurantId();
        int idOrder = order.getIdOrder();
        int idCourier = order.getIdCourier();
        LocalDateTime timeReadyToDelivery = LocalDateTime.now();

        int generatedId = 0;

        try {
            connect();

            statement.executeUpdate("INSERT INTO orders (restaurantId, orderId, courierId, timeReadyToDelivery, timeDelivered) "
                    + "VALUES (" + restaurantId + ", '" + idOrder + "', '" + idCourier + "', '" + timeReadyToDelivery + "', '" + timeReadyToDelivery + "');", Statement.RETURN_GENERATED_KEYS);

            result = statement.getGeneratedKeys();
            result.next();
            generatedId = result.getInt(1);

        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        } finally {
            close();
        }
        return generatedId;
    }

    public void updateDeliveredTime(int orderId) {
        try {
            connect();
            statement.executeUpdate("UPDATE orders SET timeDelivered = '" + LocalDateTime.now() + "' WHERE orderId = " + orderId);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }

    public void updateReadyTime(int orderId) {
        LocalDateTime timeReadyToDelivery = LocalDateTime.now();
        try {
            connect();
            statement.executeUpdate("UPDATE orders SET timeDelivered = '" + timeReadyToDelivery + "', timeReadyToDelivery = '" + timeReadyToDelivery + "' WHERE orderId = " + orderId);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }

    public void updateCourierId(int orderId, int courierId) {
        try {
            connect();
            statement.executeUpdate("UPDATE orders SET courierId = '" + courierId + "' WHERE orderId = " + orderId);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }

    private void connect() throws SQLException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException("Cannot find the driver in the classpath!", e);
        }

        connexion = DriverManager.getConnection(url, user, password);
        statement = connexion.createStatement();
    }

    private void close() {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException ignore) {
                log.log(Level.SEVERE, "MySQL Statement failed closing");
            }
        }
        if (connexion != null) {
            try {
                connexion.close();
            } catch (SQLException ignore) {
                log.log(Level.SEVERE, "MySQL Connection failed closing");
            }
        }
    }

    private void close(ResultSet result) {
        if (result != null) {
            try {
                result.close();
            } catch (SQLException ignore) {
                log.log(Level.SEVERE, "MySQL ResultSet failed closing");
            }
        }

        this.close();
    }
}
