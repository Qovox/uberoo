package fr.polytech;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public interface StatisticREST {

    @GET
    @Path("/avgDeliveryTime/{restaurantId}")
    Response getAverageDeliveryTime(@PathParam("restaurantId") int restaurantId);

    @GET
    @Path("/deliveryPerCoursier/{restaurantId}")
    Response getDeliveryPerCourier(@PathParam("restaurantId") int restaurantId);
}
