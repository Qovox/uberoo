package fr.polytech.business;

import org.json.JSONObject;

import java.time.LocalDateTime;

public class Order {

    private int id;

    private int restaurantId;

    private int idOrder;

    private int idCourier = 0;

    private LocalDateTime timeReadyToDelivery;

    private LocalDateTime timeDelivered;

    public Order() {
    }

    public Order(int restaurantId, int idOrder) {
        this.restaurantId = restaurantId;
        this.idOrder = idOrder;
    }

    public Order(JSONObject orderJsonObject) {
        if (orderJsonObject.has("restaurantId"))
            this.restaurantId = orderJsonObject.getInt("restaurantId");
        if (orderJsonObject.has("orderId"))
            this.idOrder = orderJsonObject.getInt("orderId");
    }

    public int getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(int restaurantId) {
        this.restaurantId = restaurantId;
    }

    public int getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(int idOrder) {
        this.idOrder = idOrder;
    }

    public LocalDateTime getTimeReadyToDelivery() {
        return timeReadyToDelivery;
    }

    public void setTimeReadyToDelivery(LocalDateTime timeReadyToDelivery) {
        this.timeReadyToDelivery = timeReadyToDelivery;
    }

    public LocalDateTime getTimeDelivered() {
        return timeDelivered;
    }

    public void setTimeDelivered(LocalDateTime timeDelivered) {
        this.timeDelivered = timeDelivered;
    }

    public int getIdCourier() {
        return idCourier;
    }

    public void setIdCourier(int idCourier) {
        this.idCourier = idCourier;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
