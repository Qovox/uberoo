package fr.polytech.business;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "couriers")
public class ListCourier {
    private List<Courier> couriers;

    public ListCourier() {
        this.couriers = new ArrayList<>();
    }

    @XmlElement(name = "courier")
    public List<Courier> getCouriers() {
        return couriers;
    }

    public void setCouriers(List<Courier> couriers) {
        this.couriers = couriers;
    }
}
