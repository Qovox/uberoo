package fr.polytech.business;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlRootElement(name = "courier")
public class Courier {
    private int id;
    private int orderDelivered;
    private long avgDeliveredTime;

    public Courier() {
        this.id = 0;
        this.orderDelivered = 0;
        this.avgDeliveredTime = 0;
    }

    public Courier(int id) {
        this.id = id;
        this.orderDelivered = 0;
        this.avgDeliveredTime = 0;
    }

    @XmlAttribute(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @XmlAttribute(name = "orderDelivered")
    public int getOrderDelivered() {
        return orderDelivered;
    }

    public void addOrderDelivered() {
        this.orderDelivered++;
    }

    @XmlAttribute(name = "avgDeliveredTime")
    public long getAvgDeliveredTime() {
        return avgDeliveredTime;
    }

    public void setAvgDeliveredTime(long avgDeliveredTime) {
        this.avgDeliveredTime = avgDeliveredTime;
    }
}
