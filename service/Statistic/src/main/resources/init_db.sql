Create table uberoo.orders(
  id INT(11) not null AUTO_INCREMENT,
  restaurantId INT(11) not null,
  courierId INT(11) not null,
  orderId INT(11) not null,
  timeReadyToDelivery VARCHAR(100) not null,
  timeDelivered VARCHAR(100) not null,
  primary key (id)
  ) ENGINE = INNODB;

insert into orders (restaurantId, courierId, orderId, timeReadyToDelivery, timeDelivered) VALUES (1, 1, 1, '2018-10-07T10:45:32', '2018-10-07T10:55:32');
Select * from orders;