package fr.polytech.outputs;

import fr.polytech.business.Payment;

import javax.xml.bind.annotation.XmlElement;

public class PaymentOutput extends JobResult {

    private Payment payment;
    private String message;

    @XmlElement(name = "payment")
    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    @XmlElement(name = "message")
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
