package fr.polytech.outputs;

import javax.xml.bind.annotation.XmlElement;

public class MessageOutput extends JobResult {

    private String message;

    @XmlElement(name = "message")
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
