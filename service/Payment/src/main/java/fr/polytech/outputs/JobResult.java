package fr.polytech.outputs;

import javax.xml.bind.annotation.XmlSeeAlso;

@XmlSeeAlso({MessageOutput.class, PaymentOutput.class})
public abstract class JobResult {
}
