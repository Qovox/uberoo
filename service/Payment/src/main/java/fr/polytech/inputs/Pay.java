package fr.polytech.inputs;

import fr.polytech.KafkaConsumerUtils;
import fr.polytech.KafkaProducerUtils;
import fr.polytech.business.EventType;
import fr.polytech.business.Payment;
import fr.polytech.business.PaymentMode;
import fr.polytech.business.PaymentStatus;
import fr.polytech.db.ConnectionJDBC;
import fr.polytech.outputs.JobResult;
import fr.polytech.outputs.MessageOutput;
import fr.polytech.outputs.PaymentOutput;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.json.JSONObject;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

@XmlType(name = "pay")
public class Pay extends JobInput {

    private static final Logger log = Logger.getLogger(Logger.class.getName());
    private ConnectionJDBC dao = new ConnectionJDBC();

    @XmlElement(name = "payment", required = true)
    private Payment payment;

    @XmlElement(name = "promo_code")
    private String promoCode;

    @Override
    protected JobResult run() throws DataBaseException {
        MessageOutput result = new MessageOutput();
        PaymentOutput paymentResult = new PaymentOutput();
        String id = ((Double) Math.random()).toString();

        // Retrieve complete payment from db and update it with new info, then assign
        // the fully updated payment to this class payment attribute
        Optional<Payment> dbPayment = dao.getPaymentById(payment.getId());
        dbPayment.get().setMode(payment.getMode().toUpperCase());
        dbPayment.get().setCreditCardNumber(payment.getCreditCardNumber());
        payment = dbPayment.get();

        if (payment.getMode().equals(PaymentMode.ONLINE.getName())) {
            payment.setPaymentStatus(PaymentStatus.PAID);
            paymentResult.setPayment(payment);
            paymentResult.setMessage("Online payment validated");

            try {
                dao.updatePaymentMode(payment.getId(), PaymentMode.ONLINE);
                dao.updatePaymentStatus(payment.getId(), PaymentStatus.PAID);
                dao.updatePaymentCreditCardNumber(payment.getId(), payment.getCreditCardNumber());
            } catch (SQLException e) {
                throw new DataBaseException("Service not available (database problem)");
            }
        } else {
            paymentResult.setPayment(payment);
            paymentResult.setMessage("Cash payment validated");

            try {
                dao.updatePaymentMode(payment.getId(), PaymentMode.CASH);
                dao.updatePaymentStatus(payment.getId(), PaymentStatus.NOT_PAID);
            } catch (SQLException e) {
                throw new DataBaseException("Service not available (database problem)");
            }
        }

        JSONObject paymentJSON = payment.toJsonObject();
        log.log(Level.INFO, paymentJSON.toString(4));

        if (promoCode != null) {
            paymentJSON.put("promo_code", promoCode);
            KafkaProducerUtils.produceEvent(EventType.CONFIRM_PROMO_CODE.getTopic(), id, paymentJSON);
            ConsumerRecord<String, JSONObject> record = KafkaConsumerUtils.consumeEventWithSpecificGroupId(Arrays.asList(EventType.CONFIRM_PROMO_CODE_RESPONSE.getTopic()), id);

            switch (record.value().getString("promo_code_response")) {
                case "matching":
                    float discount = record.value().getFloat("discount");
                    payment.setAmount(payment.getAmount() * ((100 - discount) / 100));

                    try {
                        dao.updatePaymentAmount(payment.getId(), payment.getAmount());
                    } catch (SQLException e) {
                        throw new DataBaseException("Service not available (database problem)");
                    }

                    paymentResult.setMessage("Promo code valid (-" + discount + "%)");
                    break;
                case "no_matching":
                    paymentResult.setMessage("Promo code not matching");
                    break;
                case "not_existing":
                    paymentResult.setMessage("Promo code not existing");
                    break;
                default:
                    throw new DataBaseException("Service not available (promo code)");
            }
        }

        KafkaProducerUtils.produceEvent(EventType.PAYMENT_CHOSEN.getTopic(), id, paymentJSON);
        return paymentResult;
    }

    @Override
    protected void check() throws BadJobInput {
        if (payment == null || payment.getCustomerName() == null || payment.getMode() == null) {
            throw new BadJobInput("invalid arguments");
        }

        if (payment.getMode().equals(PaymentMode.ONLINE.getName()) && payment.getCreditCardNumber() == 0) {
            throw new BadJobInput("invalid arguments - online payment mode requires credit card number");
        }

        Optional<Payment> dbPayment = dao.getPaymentById(payment.getId());

        if (dbPayment.isPresent()) {
            if (dbPayment.get().getStatus().equals(PaymentStatus.PAID.getName()) || !dbPayment.get().getMode().equals("Unknown")) {
                throw new BadJobInput("payment " + payment.getId() + " already paid (" + payment.getMode() + ")");
            }

            if (!dbPayment.get().getCustomerName().equals(payment.getCustomerName())) {
                throw new BadJobInput("bad account");
            }
        } else {
            throw new BadJobInput("payment " + payment.getId() + " doesn't exist");
        }
    }

    public Payment getPayment() {
        return payment;
    }
}
