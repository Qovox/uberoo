package fr.polytech;

import fr.polytech.inputs.BadJobInput;
import fr.polytech.inputs.DataBaseException;
import fr.polytech.inputs.JobInput;
import fr.polytech.outputs.JobResult;

import javax.jws.WebService;

@WebService(targetNamespace = "http://informatique.polytech.unice.fr/soa1/payment/public",
        portName = "PaymentPort",
        serviceName = "PaymentService",
        endpointInterface = "fr.polytech.PaymentDoc")
public class PaymentDocImpl implements PaymentDoc {

    @Override
    public JobResult dispatch(JobInput input) throws BadJobInput, DataBaseException {
        return input.process();
    }
}
