package fr.polytech.db;

import fr.polytech.business.Payment;
import fr.polytech.business.PaymentMode;
import fr.polytech.business.PaymentStatus;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectionJDBC {

    private static final Logger log = Logger.getLogger(Logger.class.getName());

    // Db config
    private static ResourceBundle bundle = ResourceBundle.getBundle("configDB");
    private static String urlParameters = "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false";
    private static String url = bundle.getString("jdbc.url") + bundle.getString("jdbc.databaseName") + urlParameters;
    private static String user = bundle.getString("jdbc.user");
    private static String password = bundle.getString("jdbc.password");

    // Db connection handling
    private Connection connexion = null;
    private Statement statement = null;

    public ConnectionJDBC() {
        init();
    }

    private void init() {

    }

    private void connect() throws SQLException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            System.out.println("Driver loaded!");
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException("Cannot find the driver in the classpath!", e);
        }

        connexion = DriverManager.getConnection(url, user, password);
        statement = connexion.createStatement();
    }

    private void close(ResultSet result) {
        if (result != null) {
            try {
                result.close();
            } catch (SQLException ignore) {
                log.log(Level.SEVERE, "MySQL ResultSet failed closing");
            }
        }
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException ignore) {
                log.log(Level.SEVERE, "MySQL Statement failed closing");
            }
        }
        if (connexion != null) {
            try {
                connexion.close();
            } catch (SQLException ignore) {
                log.log(Level.SEVERE, "MySQL Connection failed closing");
            }
        }
    }

    public Boolean addPayment(int id, String customerName, List<Integer> productIds, float amount) {
        try {
            connect();
            int result;

            result = statement.executeUpdate("INSERT INTO payments (id, customer_name, mode, status, amount) "
                    + "VALUES (" + id + ", '" + customerName + "', 'UNKNOWN', 'NOT_PAID', " + amount + ");");

            if (result != 1) {
                log.log(Level.WARNING, "MySQL Request - couldn't update paymentdb. Adding payment failed.");
                return false;
            }

            for (int i : productIds) {
                result = statement.executeUpdate("INSERT INTO payment_items_binding (id, item_id) "
                        + "VALUES (" + id + ", " + i + ");");

                if (result != 1) {
                    log.log(Level.WARNING, "MySQL Request - couldn't update paymentdb. Binding item " + i + "to payment " + id + " failed.");
                    return false;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            close(null);
        }

        return true;
    }

    public Optional<Payment> getPaymentById(int id) {
        ResultSet result = null;
        Payment payment;

        try {
            connect();

            result = statement.executeQuery("SELECT id, customer_name, mode, status, credit_card_number, amount FROM payments WHERE id = " + id + ";");
            result.next();

            int paymentId = result.getInt("id");
            String customerName = result.getString("customer_name");
            Optional<PaymentMode> paymentMode = PaymentMode.findByName(result.getString("mode"));
            Optional<PaymentStatus> paymentStatus = PaymentStatus.findByName(result.getString("status"));
            long creditCardNumber = result.getLong("credit_card_number");
            float amount = result.getFloat("amount");

            if (paymentMode.isPresent() && paymentStatus.isPresent()) {
                payment = new Payment(paymentId, customerName, paymentMode.get(), paymentStatus.get(), amount);

                if (paymentMode.get() == PaymentMode.ONLINE && paymentStatus.get() == PaymentStatus.PAID && creditCardNumber == 0) {
                    payment = null;
                    log.log(Level.WARNING, "MySQL Request - online payment but credit card number not found");
                } else {
                    payment.setCreditCardNumber(creditCardNumber);
                }
            } else {
                payment = null;
                log.log(Level.WARNING, "MySQL Request - payment mode or payment status corrupted");
            }

            if (payment != null) {
                result = statement.executeQuery("SELECT id, item_id FROM payment_items_binding WHERE id = " + id + ";");
                List<Integer> productIds = new ArrayList<>();

                while (result.next()) {
                    productIds.add(result.getInt("item_id"));
                }

                payment.setProductIds(productIds);
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, "MySQL Request failed - " + e.getMessage());
            payment = null;
        } finally {
            close(result);
        }

        return payment == null ? Optional.empty() : Optional.of(payment);
    }

    public boolean updatePaymentMode(int id, PaymentMode mode) throws SQLException {
        try {
            connect();
            statement.executeUpdate("UPDATE payments SET mode = '" + mode.name() + "' WHERE id = " + id);
        } catch (SQLException e) {
            log.log(Level.SEVERE, "MySQL Request failed - " + e.getMessage());
            return false;
        } finally {
            close(null);
        }
        return true;
    }

    public boolean updatePaymentStatus(int id, PaymentStatus status) throws SQLException {
        try {
            connect();
            statement.executeUpdate("UPDATE payments SET status = '" + status.name() + "' WHERE id = " + id);
        } catch (SQLException e) {
            log.log(Level.SEVERE, "MySQL Request failed - " + e.getMessage());
            return false;
        } finally {
            close(null);
        }
        return true;
    }

    public boolean updatePaymentCreditCardNumber(int id, long creditCardNumber) throws SQLException {
        try {
            connect();
            statement.executeUpdate("UPDATE payments SET credit_card_number = '" + creditCardNumber + "' WHERE id = " + id);
        } catch (SQLException e) {
            log.log(Level.SEVERE, "MySQL Request failed - " + e.getMessage());
            return false;
        } finally {
            close(null);
        }
        return true;
    }

    public boolean updatePaymentAmount(int id, float amount) throws SQLException {
        try {
            connect();
            statement.executeUpdate("UPDATE payments SET amount = '" + amount + "' WHERE id = " + id);
        } catch (SQLException e) {
            log.log(Level.SEVERE, "MySQL Request failed - " + e.getMessage());
            return false;
        } finally {
            close(null);
        }
        return true;
    }
}
