package fr.polytech.business;

import java.util.Optional;

public enum PaymentStatus {

    // Promotional code item type
    PAID("Paid"), NOT_PAID("Not paid");

    private String name;

    PaymentStatus(String name) {
        this.name = name;
    }

    public static Optional<PaymentStatus> findByName(String name) {
        for (PaymentStatus i : PaymentStatus.values()) {
            if (i.name().equals(name)) {
                return Optional.of(i);
            }
        }

        return Optional.empty();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
