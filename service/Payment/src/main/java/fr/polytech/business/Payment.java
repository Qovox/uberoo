package fr.polytech.business;

import org.json.JSONObject;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlType
@XmlRootElement(name = "payment")
public class Payment {

    private int id;
    private String customerName;
    private PaymentMode mode = PaymentMode.UNKNOWN;
    private PaymentStatus status = PaymentStatus.NOT_PAID;
    private long creditCardNumber;
    private List<Integer> productIds;
    private float amount;

    public Payment() {

    }

    public Payment(int id, String customerName, PaymentMode mode, PaymentStatus status, float amount) {
        this.id = id;
        this.customerName = customerName;
        this.mode = mode;
        this.status = status;
        this.creditCardNumber = 0;
        this.amount = amount;
    }

    public Payment(int id, String customerName, PaymentMode mode, PaymentStatus status, long creditCardNumber, float amount) {
        this.id = id;
        this.customerName = customerName;
        this.mode = mode;
        this.status = status;
        this.creditCardNumber = creditCardNumber;
        this.amount = amount;
    }

    @XmlElement(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @XmlElement(name = "customer_name")
    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    @XmlElement(name = "mode")
    public String getMode() {
        return mode.getName();
    }

    public void setMode(String mode) {
        this.mode = PaymentMode.findByName(mode).get();
    }

    public void setPaymentMode(PaymentMode mode) {
        this.mode = mode;
    }

    @XmlElement(name = "status")
    public String getStatus() {
        return status.getName();
    }

    public void setStatus(String status) {
        this.status = PaymentStatus.findByName(status).get();
    }

    public void setPaymentStatus(PaymentStatus status) {
        this.status = status;
    }

    @XmlElement(name = "credit_card_number")
    public long getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(long creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    @XmlElement(name = "amount")
    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    @XmlElementWrapper
    @XmlElement(name = "product_ids", required = true)
    public List<Integer> getProductIds() {
        return productIds;
    }

    public void setProductIds(List<Integer> productIds) {
        this.productIds = productIds;
    }

    public JSONObject toJsonObject() {
        JSONObject paymentJsonObject = new JSONObject();

        paymentJsonObject.put("id", this.id);
        paymentJsonObject.put("customer_name", this.customerName);
        paymentJsonObject.put("mode", this.mode.name());
        paymentJsonObject.put("status", this.status.name());
        paymentJsonObject.put("credit_card_number", this.creditCardNumber);
        paymentJsonObject.put("amount", this.amount);
        paymentJsonObject.put("product_ids", this.productIds);

        return paymentJsonObject;
    }
}
