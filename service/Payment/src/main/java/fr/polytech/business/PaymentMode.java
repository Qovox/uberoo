package fr.polytech.business;

import java.util.Optional;

public enum PaymentMode {

    // Promotional code item type
    ONLINE("Online"), CASH("Cash"), UNKNOWN("Unknown");

    private String name;

    PaymentMode(String name) {
        this.name = name;
    }

    public static Optional<PaymentMode> findByName(String name) {
        for (PaymentMode i : PaymentMode.values()) {
            if (i.name().equals(name)) {
                return Optional.of(i);
            }
        }

        return Optional.empty();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
