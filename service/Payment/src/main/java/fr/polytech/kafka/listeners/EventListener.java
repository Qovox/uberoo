package fr.polytech.kafka.listeners;

import fr.polytech.KafkaConsumerUtils;
import fr.polytech.KafkaProducerUtils;
import fr.polytech.business.EventType;
import fr.polytech.business.PaymentStatus;
import fr.polytech.business.ServiceType;
import fr.polytech.db.ConnectionJDBC;
import fr.polytech.listeners.KafkaEventListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EventListener implements KafkaEventListener {

    private static final Logger log = Logger.getLogger(Logger.class.getName());

    private ConnectionJDBC dao;
    private List<Thread> threads = new ArrayList<>();

    public void startListeners() {
        Thread createOrderListener = new Thread(() -> {
            startCreateOrderListener();
        });
        Thread catalogOrderAmountListener = new Thread(() -> {
            startCatalogOrderAmountListener();
        });
        Thread orderDeliveredListener = new Thread(() -> {
            startOrderDeliveredListener();
        });

        createOrderListener.start();
        catalogOrderAmountListener.start();
        orderDeliveredListener.start();

        threads.add(createOrderListener);
        threads.add(catalogOrderAmountListener);
        threads.add(orderDeliveredListener);

        dao = new ConnectionJDBC();
    }

    public void stopListeners() {
        for (Thread thread : threads) {
            thread.interrupt();
        }
    }

    public void startCreateOrderListener() {
        while (true) {
            ConsumerRecords<String, JSONObject> consumerRecords = KafkaConsumerUtils.consumeEvents(Arrays.asList(EventType.ORDER_CREATED.getTopic()), ServiceType.PAYMENT);
            for (ConsumerRecord<String, JSONObject> consumerRecord : consumerRecords) {
                processOrderCreated(consumerRecord.key(), consumerRecord.value());
            }
        }
    }

    public void startCatalogOrderAmountListener() {
        while (true) {
            ConsumerRecords<String, JSONObject> consumerRecords = KafkaConsumerUtils.consumeEvents(Arrays.asList(EventType.GET_ORDER_AMOUNT_RESPONSE.getTopic()), ServiceType.PAYMENT);
            for (ConsumerRecord<String, JSONObject> consumerRecord : consumerRecords) {
                processCatalogOrderAmount(consumerRecord.key(), consumerRecord.value());
            }
        }
    }

    public void startOrderDeliveredListener() {
        while (true) {
            ConsumerRecords<String, JSONObject> consumerRecords = KafkaConsumerUtils.consumeEvents(Arrays.asList(EventType.ORDER_DELIVERED.getTopic()), ServiceType.PAYMENT);
            for (ConsumerRecord<String, JSONObject> consumerRecord : consumerRecords) {
                processOrderDelivered(consumerRecord.key(), consumerRecord.value());
            }
        }
    }

    public void processOrderCreated(String id, JSONObject json) {
        // Assume there cannot be another payment with the same id;
        KafkaProducerUtils.produceEvent(EventType.GET_ORDER_AMOUNT.getTopic(), id, json);
    }

    public void processCatalogOrderAmount(String id, JSONObject json) {
        int orderId = json.getInt("orderId");
        String customerName = json.getString("customerName");
        List<Integer> productIds = new ArrayList<>();
        JSONArray productsIdJsonArray = json.getJSONArray("productIds");

        for (int i = 0; i < productsIdJsonArray.length(); ++i) {
            productIds.add(productsIdJsonArray.getInt(i));
        }

        float amount = json.getFloat("amount");
        boolean result = dao.addPayment(orderId, customerName, productIds, amount);

        if (!result) {
            // ???
            log.log(Level.SEVERE, "Couldn't add payment in database!");
        }
    }

    public void processOrderDelivered(String id, JSONObject json) {
        int orderId = json.getInt("orderId");
        int courierId = json.getInt("courierId");

        try {
            dao.updatePaymentStatus(orderId, PaymentStatus.PAID);
        } catch (SQLException e) {
            log.log(Level.SEVERE, "Service not available (database problem)");
        }

        log.log(Level.INFO, "Courier " + courierId + " successfully paid");
    }
}
