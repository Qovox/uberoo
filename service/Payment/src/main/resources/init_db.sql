DROP TABLE IF EXISTS payments;

CREATE TABLE payments (
  id INT(10) NOT NULL,
  customer_name VARCHAR(30) NOT NULL,
  mode VARCHAR(10) NOT NULL,
  status VARCHAR(10) NOT NULL,
  credit_card_number INT8(12),
  amount FLOAT(10),
  PRIMARY KEY (id)
) ENGINE=InnoDB;

CREATE TABLE payment_items_binding (
  id INT(10) NOT NULL,
  item_id INT(10) NOT NULL,
  PRIMARY KEY (id, item_id),
  FOREIGN KEY (id) REFERENCES payments(id)
) ENGINE=InnoDB;

/* Previous mocked payments */
INSERT IGNORE INTO payments (id, customer_name, mode, status) VALUES (1, 'Seb', 'UNKNOWN', 'NOT_PAID');
INSERT IGNORE INTO payments (id, customer_name, mode, status, credit_card_number, amount) VALUES (2, 'Alice', 'ONLINE', 'PAID', 123456789012, 11.75);
INSERT IGNORE INTO payments (id, customer_name, mode, status) VALUES (3, 'John', 'UNKNOWN', 'NOT_PAID');

INSERT IGNORE INTO payment_items_binding (id, item_id) VALUES (1, 5);
INSERT IGNORE INTO payment_items_binding (id, item_id) VALUES (2, 4);
INSERT IGNORE INTO payment_items_binding (id, item_id) VALUES (2, 6);
INSERT IGNORE INTO payment_items_binding (id, item_id) VALUES (3, 3);

SELECT * FROM payments;
SELECT * FROM payment_items_binding;