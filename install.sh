#!/bin/sh

cd service
mvn clean install

cd ..
cp -f service/Catalog/target/catalog.war docker/Catalog/
cp -f service/Catalog/target/classes/init_db.sql docker/CatalogDB/
cp -f service/Courier/target/courier.war docker/Courier/
cp -f service/Courier/target/classes/init_db.sql docker/CourierDB/
cp -f service/ETA/target/eta.war docker/ETA/
cp -f service/Order/target/order.war docker/Order/
cp -f service/Order/target/classes/init_db.sql docker/OrderDB/
cp -f service/Statistic/target/statistic.war docker/Statistic/
cp -f service/Statistic/target/classes/init_db.sql docker/StatisticDB/
cp -f service/Payment/target/payment.war docker/Payment/
cp -f service/Payment/target/classes/init_db.sql docker/PaymentDB/
cp -f service/GeoTracking/target/geotracking.war docker/GeoTracking/

docker build -t glassfish:5 ./docker/GlassFish