#!/bin/sh

HOST_CATALOG="http://localhost:8080/catalog"
HOST_COURIER="http://localhost:8081/courier"
HOST_ETA="http://localhost:8082/eta"
HOST_ORDER="http://localhost:8083/order"
HOST_STATS="http://localhost:8084/statistic"
HOST_PAYMENT="http://localhost:8085/payment"
HOST_GEOTRACK="http://localhost:8086/geotracking"



echo "###############################################"
echo "Load test is starting"
echo "###############################################"

waitService() {
    local response=`curl --write-out %{http_code} --silent --output /dev/null  $1`
    while [ $response -ne "200" ] && [ $response -ne "301" ]; do
        echo "Waiting for service $2..."
        sleep 3
        response=`curl --write-out %{http_code} --silent --output /dev/null  $1`
    done
}

waitService $HOST_CATALOG catalog
waitService $HOST_COURIER courier
waitService $HOST_ETA eta
waitService $HOST_ORDER order
waitService $HOST_STATS statistic
waitService $HOST_PAYMENT payment
waitService $HOST_GEOTRACK geotracking

echo "All service is up\n"

cd ./stress
mvn clean package
mvn gatling:execute